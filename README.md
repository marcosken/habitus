<h1 align="center">
    <img src="./src/assets/img/logo-desktop.svg">
</h1>

## Índice

- **[Sobre o Projeto](#-sobre-o-projeto)**
- **[Demonstração](#-demonstração)**
- **[Funcionalidades](#-funcionalidades)**
- **[Layout](#-layout)**
- **[Tecnologias](#-tecnologias)**
- **[Equipe](#-equipe)**


## 🖥️ Sobre o Projeto

> Projeto criado dentro do curso da **Kenzie Academy Brasil**

**Habitus** é uma plataforma de gestão de hábitos.

Através de uma interface amigável, seu objetivo é fazer com que o usuário possa administrar qualquer atividade que ele deseje tornar um hábito.

Além de poder cadastrar e acompanhar suas atividades de maneira simples, o usuário tem a possibilidade de participar de grupos com outros integrantes que compartilham metas semelhantes.

## 🚀 Demonstração

Deploy da aplicação: [![Vercel](https://img.shields.io/badge/vercel-%23000000.svg?style=for-the-badge&logo=vercel&logoColor=white)](https://habitus-front.vercel.app/)

<h1 align="center">
    <img src="./src/assets/home.png" width=700/>
</h1>


## 💡 Funcionalidades

- [x] Cadastro de usuário
- [x] Login de usuário
- [x] Edição de dados do usuário
- [x] Cadastro de hábitos
- [x] Exibição de hábitos cadastrados
- [x] Acompanhamento do progresso dos hábitos
- [x] Edição de hábitos
- [x] Exclusão de hábitos
- [x] Criação de grupos
- [x] Exibição de todos os grupos
- [x] Participar de grupos
- [x] Exibição dos grupos que o usuário participa
- [x] Criação de metas para o grupo
- [x] Exibição das metas do grupo
- [x] Acompanhamento do progresso das metas do grupo
- [x] Edição de metas do grupo
- [x] Exclusão de metas do grupo

## 🎨 Layout

O layout da aplicação está disponível no Figma: [![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white)](https://www.figma.com/file/lVeK6rEjPluUNLmjslS6pZ/Habitus?node-id=0%3A1&t=8rcUkA2dRTey1F1K-0)

<p align="center">
  <img src="./src/assets/login.png" width=500>

  <img src="./src/assets/dashboard.png" width=500>

  <img src="./src/assets/habitForm.png" width=500>

  <img src="./src/assets/groups.png" width=500>
</p>



## 🛠️ Tecnologias

Para o desenvolvimento desse projeto, as seguintes ferramentas foram utilizadas:

#### Front-end

- [React.js](https://pt-br.reactjs.org/)
- [Axios](https://axios-http.com/ptbr/)
- [React Hook Form](https://react-hook-form.com/)
- [Styled Components](https://styled-components.com/)
- [React Icons](https://react-icons.github.io/react-icons/)
- [React Modal](https://www.npmjs.com/package/react-modal)
- [React Responsive Carousel](http://react-responsive-carousel.js.org/)
- [Date fns](https://date-fns.org/)

#### Back-end

- [Habits API](https://habit-docs.vercel.app/)


## 👥 Equipe

- José Flávio: [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/jotaftm/)
- Marcos Kuribayashi: [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/marcos-kuribayashi/)
- Maykel Nekel: [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/maykelnekel/)
- Rafael Maier: [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/rafaelrmaier/)
- Raphael Marum: [![LinkedIn](https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/raphaelmarum/)
