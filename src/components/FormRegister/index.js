import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useHistory } from "react-router-dom";
import Input from "../input";
import { FaUserCircle } from "react-icons/fa";
import { MdEmail } from "react-icons/md";
import { GoKey } from "react-icons/go";
import { useUser } from "../../providers/user";
import { Link } from "react-router-dom";
import { Container, Form, ButtonContainer, Span } from "./styles";
import { Button } from "../button";

const FormRegister = () => {
  const { registerNewUser } = useUser();

  const history = useHistory();

  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    email: yup.string().required("Campo obrigatório").email("E-mail inválido"),
    password: yup.string().required("Campo obrigatório"),
    confirmPassword: yup
      .string()
      .oneOf([yup.ref("password")], "As senhas não coincidem")
      .required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const handleForm = ({ username, email, password }) => {
    const user = { username, email, password };
    registerNewUser(user, history);
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit(handleForm)}>
        <div>
          <Input
            label="Nome de usuário"
            placeholder="Nome de usuário"
            icon={FaUserCircle}
            name="username"
            register={register}
            error={errors.username?.message}
          />
        </div>
        <div>
          <Input
            label="E-mail"
            placeholder="E-mail"
            icon={MdEmail}
            name="email"
            register={register}
            error={errors.email?.message}
          />
        </div>
        <div>
          <Input
            label="Senha"
            placeholder="Senha"
            icon={GoKey}
            name="password"
            type="password"
            register={register}
            error={errors.password?.message}
          />
        </div>
        <div>
          <Input
            label="Confirmar senha"
            placeholder="Confirmar senha"
            icon={GoKey}
            name="confirmPassword"
            type="password"
            register={register}
            error={errors.confirmPassword?.message}
          />
        </div>
        <ButtonContainer>
          <Button className="registerAndLogin" type="submit">
            CADASTRE-SE
          </Button>
        </ButtonContainer>
      </Form>
      <Span>
        Já possui conta? Faça o <Link to="/login">Login</Link>.
      </Span>
    </Container>
  );
};

export default FormRegister;
