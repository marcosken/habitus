import { useState } from "react";
import { useMyGroups } from "../../providers/myGroups";
import Modal from "react-modal";
import GroupDetails from "../groupDetails";
import FormEditGroup from "../formEditGroup";
import {
  Container,
  Name,
  Category,
  Description,
  Figure,
  Image,
  ButtonsContainer,
} from "./styles";
import openArms from "../../assets/img/openArms.svg";
import { Button } from "../button";
import { useHistory } from "react-router-dom";
import { useUser } from "../../providers/user";

const GroupCard = ({ group, noButton, groupClass = "" }) => {
  const history = useHistory();

  const { subscribeToGroup } = useMyGroups();
  const [isOpen, setIsOpen] = useState(false);
  const [editIsOpen, setEditIsOpen] = useState(false);
  const { accessToken } = useUser();

  const toggleEditModal = () => {
    setEditIsOpen(!editIsOpen);
  };

  const toggleModal = () => {
    setIsOpen(!isOpen);
  };

  // const isCreator = isGroupCreator(group);

  return (
    <Container className={groupClass}>
      <Figure className={groupClass}>
        <Image
          className={groupClass}
          src={openArms}
          alt="pessoal com braços abertos"
        />
      </Figure>
      <Name className={groupClass}>{group.name}</Name>
      <Category className={groupClass}>
        <span>CATEGORIA: </span>
        {group.category}
      </Category>

      <Description className={groupClass}>
        <span>DESCRIÇÃO: </span>
        {group.description}
      </Description>

      <ButtonsContainer>
        {noButton ? null : (
          <Button
            classNameProp="subscribeGroup"
            onClick={() => subscribeToGroup(group, accessToken)}
          >
            Participar
          </Button>
        )}
        <Button
          classNameProp="DetailsGroupCard"
          onClick={() => history.push(`/specificGroup/${group.id}`)}
        >
          ver mais
        </Button>
      </ButtonsContainer>
      <Modal isOpen={isOpen} ariaHideApp={false} onRequestClose={toggleModal}>
        <Button style={{ float: "right" }} onClick={toggleModal}>
          X
        </Button>
        <GroupDetails actual={group} />
        {/* {isCreator && <Button onClick={toggleEditModal}>Editar</Button>} */}
      </Modal>

      <Modal
        isOpen={editIsOpen}
        ariaHideApp={false}
        onRequestClose={toggleEditModal}
      >
        <Button style={{ float: "right" }} onClick={toggleEditModal}>
          X
        </Button>
        <FormEditGroup actual={group} />
      </Modal>
    </Container>
  );
};

export default GroupCard;
