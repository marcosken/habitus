import styled from "styled-components";

export const Container = styled.div.attrs((props) => ({
  className: props.className,
}))`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  background-color: var(--mono8);
  width: 200px;
  height: 250px;
  font-family: var(--font1);
  color: var(--mono4);
  padding: 1.2rem 0.8rem;
  margin: 1rem 1rem;
  border-radius: 14px;
  box-shadow: 0 0 10px 2px var(--mono5);

  @media (max-width: 768px) {
    &.mobileGroups {
      margin: 1rem 0rem;
      width: 100vw;
      height: 186px;
      border-radius: 0;
      padding: 1rem;
      line-height: 1rem;
    }
  }
`;

export const Name = styled.h2.attrs((props) => ({
  className: props.className,
}))`
  text-align: center;
  font-size: 1.4rem;
  font-weight: bold;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  @media (max-width: 768px) {
    &.mobileGroups {
      font-size: 1.2rem;
    }
  }
`;

export const Category = styled.h2.attrs((props) => ({
  className: props.className,
}))`
  font-size: 1rem;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  span {
    font-weight: bold;
    font-size: 0.9rem;
  }
  @media (max-width: 768px) {
    &.mobileGroups {
      font-size: 0.8rem;
    }
  }
`;

export const Description = styled.h2.attrs((props) => ({
  className: props.className,
}))`
  font-size: 1rem;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  span {
    font-weight: bold;
    font-size: 0.9rem;
  }
  @media (max-width: 768px) {
    &.mobileGroups {
      font-size: 0.8rem;
    }
  }
`;

// export const DescriptionContainer = styled.div.attrs((props) => ({
//   className: props.className,
// }))`
//   height: 55%;
//   width: 100%;
//   overflow: hidden;
//   text-overflow: ellipsis;
//   white-space: nowrap;
//   @media (max-width: 768px) {
//     &.mobileGroups {
//       height: 1.5rem;
//     }
//   }
// `;

// export const Description = styled.p.attrs((props) => ({
//   className: props.className,
// }))`
//   line-height: 1.3rem;
//   font-size: 1rem;
//   width: 100%;
//   height: 100%;
//   overflow: hidden;
//   text-overflow: ellipsis;
//   white-space: nowrap;
//   span {
//     font-weight: bold;
//   }
//   @media (max-width: 768px) {
//     &.mobileGroups {
//       font-size: 0.8rem;
//       width: 100%;
//       white-space: nowrap;
//       height: 4rem;
//       overflow: hidden;
//       text-overflow: ellipsis;
//       white-space: normal;
//     }
//   }
// `;

export const Figure = styled.figure.attrs((props) => ({
  className: props.className,
}))`
  position: absolute;
  top: 30%;
  width: 180px;
  height: 160px;
  @media (max-width: 768px) {
    &.mobileGroups {
      display: none;
    }
  }
`;

export const Image = styled.img.attrs((props) => ({
  className: props.className,
}))`
  width: 100%;
  height: 100%;
  opacity: 50%;
  @media (max-width: 768px) {
    &.mobileGroups {
      display: none;
    }
  }
`;
export const ButtonsContainer = styled.div.attrs((props) => ({
  className: props.className,
}))`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  @media (min-width: 768px) {
    flex-direction: column;
    position: relative;
    gap: 0.5rem;
  }
`;
