import { useGroups } from "../../providers/groups";
import GroupCard from "../GroupCard";
import { BiLeftArrow, BiRightArrow } from "react-icons/bi";
import {
  Container,
  ArrowsNavPrev,
  ArrowsNavNext,
  ArrowsNavContent,
  ContainerGroups,
} from "./styles";

const GroupsList = () => {
  const { groups, page, setPage, countGroups } = useGroups();

  const prev = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  const next = () => {
    if (page < Math.ceil(countGroups / 15)) {
      setPage(page + 1);
    }
  };

  return (
    <Container>
      {/* {isLoading && (
        <img style={{ textAlign: "center" }} src={loading} alt="" />
      )} */}
      <ArrowsNavContent>
        <ArrowsNavPrev page={page}>
          <BiLeftArrow onClick={prev} />
        </ArrowsNavPrev>
        <ArrowsNavNext page={page} countGroups={countGroups}>
          <BiRightArrow onClick={next} />
        </ArrowsNavNext>
      </ArrowsNavContent>
      <ContainerGroups>
        {groups?.map((group) => (
          <GroupCard groupClass="mobileGroups" key={group.id} group={group} />
        ))}
      </ContainerGroups>
      <ArrowsNavContent>
        <ArrowsNavPrev page={page}>
          <BiLeftArrow onClick={prev} />
        </ArrowsNavPrev>
        <ArrowsNavNext page={page} countGroups={countGroups}>
          <BiRightArrow onClick={next} />
        </ArrowsNavNext>
      </ArrowsNavContent>
    </Container>
  );
};

export default GroupsList;
