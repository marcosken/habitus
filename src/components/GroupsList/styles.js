import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column wrap;
`;

export const ContainerGroups = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;

export const ArrowsNavPrev = styled.div`
  font-weight: bold;
  color: ${(prop) => (prop.page === 1 ? "transparent" : "var(--mono4)")};
  font-size: 2rem;
  cursor: ${(prop) => (prop.page === 1 ? "default" : "pointer")};

  :hover {
    opacity: 0.8;
  }
`;

export const ArrowsNavNext = styled.div`
  font-weight: bold;
  color: ${(prop) =>
    prop.page === Math.ceil(prop.countGroups / 15)
      ? "transparent"
      : "var(--mono4)"};
  font-size: 2rem;
  cursor: ${(prop) =>
    prop.page === Math.ceil(prop.countGroups / 15) ? "default" : "pointer"};

  :hover {
    opacity: 0.8;
  }
`;

export const ArrowsNavContent = styled.div`
  width: 40%;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
`;
