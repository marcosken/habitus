import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Input from "../input";
import { IoMdSend } from "react-icons/io";
import { 
  Container,
  ChangeInfo, 
  SendInfo,
  ContainerButtons, 
  Inputs,
 } from "./styles";
import { useActivities } from "../../providers/activities";
import { format, compareAsc } from "date-fns";
import { Button } from "../button";

const Activity = ({ actual }) => {
  const currDate = format(new Date(), "yyyy/MM/dd");

  const { register, handleSubmit } = useForm();

  const [title, setTitle] = useState("");
  const [actualTitle, setActualTitle] = useState(actual.title);
  const { editActivity, deleteActivity, checkActivity } = useActivities();

  const [readOnly, setReadOnly] = useState(true);

  const switchReadOnly = () => {
    setReadOnly(readOnly === true ? false : true);
  };

  const handleClickOnCheck = () => {
    setActualTitle(title);
    editActivity({ title: title }, actual.id);
    switchReadOnly();
    setTitle("");
  };

  useEffect(() => {

  }, [actualTitle])

  return (
    <Container>
      <Inputs>
        <ChangeInfo onClick={switchReadOnly}>
          <Input
            register={register}
            name="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            label={actualTitle}
            readOnly={readOnly}
          />
        </ChangeInfo>
        {!readOnly && (
          <SendInfo onClick={handleClickOnCheck}>
            Enviar <IoMdSend style={{ marginLeft: "0.5rem"}} />
          </SendInfo>
        )}
      </Inputs>

        <ContainerButtons>
          <Button
            classNameProp="deleteHabit cardHabit"
            onClick={() => deleteActivity(actual.id)}
          >
            EXCLUIR
          </Button>

          <Button
            classNameProp="checkHabit cardHabit"
            onClick={() => checkActivity(actual)}
          >
            CHECK
          </Button>
        </ContainerButtons>
    </Container>
  );
};

export default Activity;
