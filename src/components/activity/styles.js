import styled from "styled-components";

export const Container = styled.div `
  width: 100vw;
  max-width: 425px;
  height: 120px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  background-color: var(--mono8);
  box-shadow: 0px 3px 8px rgba(0, 0, 0, 0.24);
  margin: 0.5rem;
  background-color: var(--mono3);
  border-radius: 1rem;
`;

export const Inputs = styled.div `
  display: flex;
  align-items: center;
  width: 90%;
`;

export const ContainerButtons = styled.div `
    width: 90%;
    margin: 0 auto;
    display: flex;
    justify-content: space-around;
`;

export const ChangeInfo = styled.div `
  border: none;
  display: flex;
  color: var(--mono4);
  font-size: 1.2rem;
  display: flex;
  flex-direction: column;
  width: 75%;
`;

export const SendInfo = styled.button `
  border: none;
  display: flex;
  color: var(--mono4);
  font-size: 1.2rem;
  margin-bottom: -1.5rem;
  height: 2rem;
  align-items: center;
  border-radius: 0 0.5rem 0.5rem 0;
`;