import { format } from 'date-fns';
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Activity from "../activity";
import Input from "../input";
import { ContainerActivities, ContainerSearch } from "./styles"
import { BsSearch } from "react-icons/bs";


const ActivityList = ({currentGroup}) => {

    const [searchResults, setSearchResults] = useState('');

    // const [currDate, setCurrDate] = useState(format(new Date(), "yyyy-MM-dd") +"T00:00:00Z");

    // const [todaysActivities, setTodaysActivities] = useState(currentGroup
    //     .activities?.filter((el) => el.realization_time === currDate));

    // console.log(currentGroup.activities);


    const { register } = useForm();

    const [userInput, setUserInput] = useState("");

    useEffect(() => {
        if(userInput){
            setSearchResults(currentGroup.activities?.filter(el => el.title.toLowerCase().includes(userInput.toLowerCase())))
;
        }else{
            setSearchResults("");
        }
    }, [userInput])


    return(
        <>
            <ContainerSearch><Input register={register} name="userInput" value={userInput} onChange={(e) => setUserInput(e.target.value)} label="Pesquisar atividade" icon={BsSearch} /></ContainerSearch>
            <div>
                
                {searchResults?
                searchResults.map((el, index) => <Activity key={index} actual={el}/>)
                :
                <ContainerActivities>
                   { currentGroup.activities?.map((el, index) => <Activity key={index} actual={el}/>)}

                </ContainerActivities>
                }
            </div>
            
        </>
        
    )

    }
        

export default ActivityList;