import styled from "styled-components";

export const ContainerActivities = styled.div `
    margin: 1rem auto;

    @media (min-width: 425px) {
        width: 95vw;
        display: flex;
        flex-wrap: wrap;
        margin: 0 auto;
    }
`

export const ContainerSearch = styled.div `
    width: 290px;
    margin: 1rem auto;

    @media (min-width: 768px) {
        width: 400px;
    }
`