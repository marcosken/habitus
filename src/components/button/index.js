import { ButtonComponent } from "./styles";

export const Button = ({
  activitySelected,
  goalSelected,
  classNameProp,
  children,
  disabled,
  ...rest
}) => {
  return (
    <ButtonComponent
      className={classNameProp}
      type="button"
      disabled={disabled}
      {...rest}
    >
      {children}
    </ButtonComponent>
  );
};
