import styled from "styled-components";

export const ButtonComponent = styled.button.attrs((props) => ({
  className: props.className,
}))`
  border: none;
  cursor: ${(prop) => (prop.disabled ? "not-allowed" : "pointer")};
  box-shadow: 0px 12px 20px -15px black;

  &.login {
    background-color: var(--mono4);
    padding: 0.7rem 1.7rem;
    border-radius: 39px;
    color: var(--mono8);
    font-family: var(--font1);
    font-weight: bold;
    font-size: 0.9rem;
    position: absolute;
    top: 8px;
    right: 16px;

    :hover {
      filter: brightness(150%);
      transform: scale(1.05);
    }
    @media (min-width: 768px) {
      padding: 0.7rem 2.8rem;
      font-size: 1rem;
    }
  }

  &.createAcc {
    background-color: var(--mono7);
    color: var(--mono8);
    padding: 0.8rem 2.8rem;
    border-radius: 39px;
    font-family: var(--font1);
    font-weight: bold;
    font-size: 1.5rem;
    border: 3px solid var(--mono8);
    :hover {
      filter: brightness(105%);
      transform: scale(1.05);
    }
    @media (min-width: 768px) {
      margin-left: 9rem;
    }
  }

  &.registerAndLogin {
    background-color: var(--mono4);
    border: 1px solid var(--mono4);
    border-radius: 0.2rem;
    font-family: var(--font2);
    color: var(--mono8);
    font-weight: bold;
    width: 100%;
    padding: 0.4rem;
    font-size: 1.3rem;
  }

  &.cardHabit {
    width: 4.5rem;
    height: 1.25rem;
    font-family: var(--font3);
    font-size: 0.6rem;

    :hover {
      filter: ${(prop) => (!prop.disabled ? "brightness(150%)" : "none")};
    }

    @media (min-width: 1024px) {
      width: 5rem;
      height: 1.3rem;
    }
  }

  &.deleteHabit {
    background-color: var(--mono8);
    color: var(--mono6);
    width: 55%;
    height: 2.5rem;
    border-radius: 1.5rem;
    font-size: 0.8rem;
    border: 1px solid var(--mono6);
    :hover {
      border: 1px solid var(--mono6);
      background-color: var(--mono6);
      color: var(--mono8);
      transform: scale(1.05);
    }
  }

  &.checkHabit {
    background-color: var(--mono7);
    color: var(--mono8);
    width: 55%;
    height: 2.5rem;
    border-radius: 1.5rem;
    font-size: 0.8rem;
    opacity: ${(prop) => (prop.disabled ? "0.30" : "none")};
    :hover {
      transform: ${(prop) => (!prop.disabled ? "scale(1.05)" : "none")};
    }
  }

  &.newHabit {
    width: 136px;
    height: 38px;
    background-color: var(--mono2);
    color: var(--mono4);
    font-family: var(--font1);
    font-weight: bold;
    border-radius: 24px;
    font-size: 1rem;
    margin-top: 10px;

    :hover {
      filter: brightness(120%);
      transform: scale(1.05);
    }
  }

  &.newGoup {
    position: relative;
    margin-left: 10rem;
    opacity: ${(prop) => (prop.disabled ? "0.30" : "none")};
    :hover {
      transform: ${(prop) => (!prop.disabled ? "scale(1.05)" : "none")};
      filter: ${(prop) => (!prop.disabled ? "brightness(150%)" : "none")};
    }
  }

  &.addBtnModal {
    background: var(--mono2);
    width: 100%;
    font-family: var(--font3);
    font-size: 1.6rem;
    font-weight: bold;
    color: var(--mono8);
    -webkit-text-stroke-width: 1px;
    -webkit-text-stroke-color: var(--mono4);
    padding: 0.4rem 0;
    border-radius: 0.2rem;
    :hover {
      filter: brightness(150%);
    }
  }

  &.DetailsGroupCard {
    background-color: var(--mono2);
    color: var(--mono4);
    text-transform: uppercase;
    font-weight: bold;
    height: 2.5rem;
    width: 7.5rem;
    border-radius: 1.5rem;

    :hover {
      filter: brightness(110%);
      transform: scale(1.05);
    }
  }

  &.subscribeGroup {
    background-color: var(--mono7);
    color: var(--mono8);
    text-transform: uppercase;
    font-weight: bold;
    height: 2.5rem;
    width: 7.5rem;
    font-size: 0.8rem;
    border-radius: 1.5rem;
    :hover {
      filter: brightness(110%);
      transform: scale(1.05);
    }
  }

  &.addGoal,
  &.addActivity {
    padding: 0.7rem 0.9rem;
    background-color: var(--mono2);
    color: var(--mono8);
    font-weight: bold;
    border-radius: 24px;
    font-size: 0.8rem;

    :hover {
      background-color: var(--mono7);
    }
  }

  &.activityAndGoalButton {
    width: 150px;
    background-color: var(--mono8);
    color: var(--mono4);
    background-color: ${(prop) => prop.selected && "var(--mono4)"};
    color: ${(prop) => prop.selected && "var(--mono8)"};
    border: 2px solid var(--mono4);
    padding: 0.7rem 0.9rem;
    font-weight: bold;
    border-radius: 24px;
    font-size: 1rem;

    :hover {
      filter: brightness(150%);
    }
  }

  &.check {
    margin-top: 1rem;
    background-color: var(--mono7);
    padding: 0.8rem;
    color: var(--mono8);
    font-weight: bold;
    text-transform: uppercase;
    border: 2px solid var(--mono7);
    border-radius: 30px;
    :hover {
      filter: brightness(105%);
    }
  }
  &.exit {
    margin-top: 1rem;
    padding: 0.8rem;
    background-color: var(--mono8);
    color: var(--mono6);
    border: 2px solid var(--mono6);
    font-weight: bold;
    text-transform: uppercase;
    :hover {
      background-color: var(--mono6);
      border: 2px solid var(--mono6);
      color: var(--mono8);
    }
  }
`;
