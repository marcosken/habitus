import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { Container, Form } from "./styles";
import * as yup from "yup";
import { useActivities } from "../../providers/activities";
import { Button } from "../button";
import Input from "../input";
import { format } from 'date-fns';

const FormCreateActivity = ({ toggleModal, groupID }) => {
  const { createActivity } = useActivities();

  const currDate = format(new Date(), "yyyy-MM-dd") +"T00:00:00Z";

  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const handleForm = (payload) => {
    createActivity({...payload, realization_time: currDate}, groupID);
    toggleModal();
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit(handleForm)}>
        <Input
          register={register}
          name="title"
          label="Título"
          error={errors.title?.message}
        />

        <Button classNameProp="addBtnModal" type="submit">
          Adicionar
        </Button>
      </Form>
    </Container>
  );
};

export default FormCreateActivity;
