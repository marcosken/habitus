import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  @media (min-width: 768px) {
    width: 50%;
  }
  @media (min-width: 1024px) {
    width: 30%;
  }
`;

export const Form = styled.form`
  div {
    margin-bottom: 1.5rem;
  }
`;
