import { yupResolver } from "@hookform/resolvers/yup";
import { useGoals } from "../../providers/goals";
import * as yup from "yup";
import Input from "../input";
import { useForm } from "react-hook-form";
import { Button } from "../button";
import { Container, Form } from "./styles";

const FormCreateGoal = ({ toggleModal, groupID }) => {
  const { createGoal } = useGoals();

  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    difficulty: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const handleForm = (data) => {
    
    createGoal(data, groupID);
    toggleModal();
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit(handleForm)}>
        <Input
          register={register}
          label="Título"
          name="title"
          error={errors.title?.message}
        />

        <Input
          register={register}
          label="Dificuldade"
          name="difficulty"
          error={errors.difficulty?.message}
        />
        <Button className="addBtnModal" type="submit">
          Adicionar
        </Button>
      </Form>
    </Container>
  );
};

export default FormCreateGoal;
