import Input from "../input";
import { Button } from "../button";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useMyGroups } from "../../providers/myGroups";
import { Container, Form } from "./styles";
import { useUser } from "../../providers/user";

const FormCreateGroup = ({ toggleModal }) => {
  const { addToMyGroups } = useMyGroups();
  const { accessToken } = useUser();
  const schema = yup.object().shape({
    name: yup.string().required("Campo obrigatório."),
    description: yup.string().required("Campo obrigatório."),
    category: yup.string().required("Campo obrigatório."),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const handleForm = (data) => {
    addToMyGroups(data, accessToken);
    toggleModal();
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit(handleForm)}>
        <div>
          <Input
            label="Nome"
            name="name"
            register={register}
            error={errors.name?.message}
          />
        </div>
        <div>
          <Input
            label="Descrição"
            name="description"
            register={register}
            error={errors.description?.message}
          />
        </div>
        <div>
          <Input
            label="Categoria"
            name="category"
            register={register}
            error={errors.category?.message}
          />
        </div>
        <Button className="addBtnModal" type="submit">
          Adicionar
        </Button>
      </Form>
    </Container>
  );
};

export default FormCreateGroup;
