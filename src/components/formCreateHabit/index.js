import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import Input from "../input";
import { useHabits } from "../../providers/habits";
import { Container, Form } from "./styles";
import { Button } from "../button";
import { useUser } from "../../providers/user";

const FormCreateHabit = ({ toggleModal }) => {
  const { createHabit } = useHabits();
  const { accessToken, userID } = useUser();

  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    category: yup.string().required("Campo obrigatório"),
    difficulty: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const handleForm = (data) => {
    createHabit(data, accessToken, userID);
    toggleModal();
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit(handleForm)}>
        <div>
          <Input
            label="Título"
            name="title"
            register={register}
            error={errors.title?.message}
          />
        </div>
        <div>
          <Input
            label="Categoria"
            name="category"
            register={register}
            error={errors.category?.message}
          />
        </div>
        <div>
          <Input
            label="Dificuldade"
            name="difficulty"
            register={register}
            error={errors.difficulty?.message}
          />
        </div>
        <Button className="addBtnModal" type="submit">
          Adicionar
        </Button>
      </Form>
    </Container>
  );
};

export default FormCreateHabit;
