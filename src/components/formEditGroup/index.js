import { useState } from "react";
import { appendErrors, useForm } from "react-hook-form";
import { toast } from "react-toastify";

import { useMyGroups } from "../../providers/myGroups";
import { Button } from "../button";
import Input from "../input";



const FormEditGroup = ({actual, toggleEditModal}) => {
    const { myGroups } = useMyGroups();
    const [name, setName] = useState(actual.name);
    const [description, setDescription] = useState(actual.description);
    const [category, setCategory] = useState(actual.category);

    const { editGroup } = useMyGroups();

    const { register, handleSubmit, formState: {errors}} = useForm();
    const [userIsInGroup, setUserInGroup] = useState();

    const onSubmitFunction = (payload) => {

            editGroup(payload, actual.id);
      
        toggleEditModal();
    }

    return(
        <form onSubmit={handleSubmit(onSubmitFunction)}>
            <div>
                <Input 
                register={register} 
                name="name"
                label="Nome do grupo"
                error={errors.name?.message}
                value={name}
                onChange={(e) => setName(e.target.value)}
                />
            </div>
            <div>
                <Input 
                register={register} 
                name="description"
                label="Descrição"
                error={errors.description?.message}
                value={description}
                onChange={(e) => setDescription(e.target.value)}

                />
            </div>
            <div>
                <Input 
                register={register} 
                name="category"
                label="Categoria"
                error={errors.category?.message}
                value={category}
                onChange={(e) => setCategory(e.target.value)}

                />
            </div>
        <Button className="addBtnModal" style={{width: "100px", borderRadius: "1.5rem"}} type="submit">Editar</Button>
        </form>
    )


}

export default FormEditGroup;