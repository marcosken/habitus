import { useForm } from "react-hook-form";
import { useHabits } from "../../providers/habits"
import { Button } from "../button";
import FormEditGroup from "../formEditGroup";
import Input from "../input";


const FormEditHabit = ({actual}) => {

    const { editHabit } = useHabits();

    const { register, handleSubmit } = useForm();

    const onSubmitFunction = (payload) => {
        editHabit(payload, actual.id);

    }

    return(
        <form onSubmit={handleSubmit(onSubmitFunction)}>
            <Input
            register={register}
            name="title"
            label="Título do hábito"
            value={actual.title}
            />
            <Input
            register={register}
            name="category"
            label="Categoria"
            value={actual.category}

            />
            <Input
            register={register}
            name="difficulty"
            label="Dificuldade"
            value={actual.difficulty}

            />
            <Button type="submit">Editar</Button>
        </form>
        
    )
}


export default FormEditHabit;

