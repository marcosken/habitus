import Input from "../input";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { api } from "../../services/api";
import { useState } from "react";
import { toast } from "react-toastify";
import { useUser } from "../../providers/user";

const FormEditUser = () => {
  const [error, setError] = useState(false);
  const [accessToken, setAccessToken] = useState(
    localStorage.getItem("authToken")
  );
  const { user, userID } = useUser();

  const [input, setInput] = useState({
    username: user.username,
    email: user.email,
  });

  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    email: yup.string().required("Campo obrigatório").email("E-mail inválido"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const handleForm = (data) => {
    console.log(data);
    api
      .patch(`/users/${userID}/`, data, {
        headers: { Authorization: `Bearer ${accessToken}` },
      })
      .then((_) => {
        toast.success("Alterações salvas com sucesso!");
        setError(false);
      })
      .catch((err) => {
        if (err.response.status === 400) {
          setError(true);
        }
      });
  };

  const handleChange = (e) => {
    switch (e.target.name) {
      case "username":
        setInput({ ...input, username: e.target.value });
        break;
      case "email":
        setInput({ ...input, email: e.target.value });
        break;
      default:
        break;
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit(handleForm)}>
        <div>
          <Input
            label="Nome de usuário"
            name="username"
            register={register}
            error={errors.username?.message}
            value={input.username}
            onChange={handleChange}
          />
        </div>
        <div>
          <Input
            label="E-mail"
            name="email"
            register={register}
            error={errors.email?.message}
            value={input.email}
            onChange={handleChange}
          />
        </div>
        <button type="submit">Salvar</button>
      </form>
      {error && <span>Este nome de usuário já existe!</span>}
    </div>
  );
};

export default FormEditUser;
