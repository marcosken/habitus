import { useUser } from "../../providers/user";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import Input from "../input";
import { Button } from "../button";
import { Link, useHistory } from "react-router-dom";
import { FaUserCircle } from "react-icons/fa";
import { GoKey } from "react-icons/go";
import { Container, Form, ButtonContainer, Span } from "./style";

const FormLogin = () => {
  const history = useHistory();

  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    password: yup.string().required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const { login } = useUser();

  const onSubmitFunction = (payload) => {
    login(payload, history);
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit(onSubmitFunction)}>
        <div>
          <Input
            register={register}
            icon={FaUserCircle}
            type="text"
            label="Nome de usuário"
            placeholder="Nome de usuário"
            name="username"
            error={errors.username?.message}
          />
        </div>
        <div>
          <Input
            type="password"
            icon={GoKey}
            register={register}
            label="Senha"
            placeholder="Senha"
            name="password"
            error={errors.password?.message}
          />
        </div>
        <ButtonContainer>
          <Button className="registerAndLogin" type="submit">
            ENTRAR
          </Button>
        </ButtonContainer>
      </Form>
      <Span>
        Ainda não tem conta? Faça seu <Link to="/register">cadastro</Link>!
      </Span>
    </Container>
  );
};

export default FormLogin;
