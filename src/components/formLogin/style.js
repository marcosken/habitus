import styled from "styled-components";

export const Container = styled.div`
  width: 90vw;
  font-family: var(--font1);
  font-size: 1rem;
  color: var(--mono4);

  @media (min-width: 768px) {
    width: 56vw;
  }

  @media (min-width: 1024px) {
    width: 32vw;
  }
`;

export const Span = styled.span`
  font-size: 1.2rem;
  display: block;
  padding: 0 1.5rem;
  text-align: center;
  line-height: 1.6rem;

  @media (min-width: 1024px) {
    padding: 0;
    margin-top: 4rem;
  }

  a {
    color: var(--mono9);
    text-decoration: none;
    :hover {
      text-decoration: underline;
    }
  }
`;

export const Form = styled.form`
  div {
    margin-bottom: 1rem;

    @media (min-width: 1024px) {
      margin-bottom: 3rem;
    }
  }
`;

export const ButtonContainer = styled.div`
  margin-top: 6.5rem;

  @media (min-width: 1024px) {
    margin-top: 4rem;
  }
`;
