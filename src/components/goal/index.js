import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useGoals } from "../../providers/goals";
import { Button } from "../button";
import Input from "../input";
import { IoMdSend } from "react-icons/io";
import { 
  ChangeInfo, 
  GoalTitle, 
  SendInfo, 
  Container, 
  GoalDifficulty,
  Content, 
  Buttons
} from "./style";
import { useMyGroups } from "../../providers/myGroups";

const Goal = ({ actual, userIsInGroup, currentGroup }) => {
  const { progressInGoal, editGoal, deleteGoal } = useGoals();
  const { isGroupCreator } = useMyGroups();

  const [title, setTitle] = useState(actual.title);
  const [difficulty, setDifficulty] = useState(actual.difficulty);
  const { register } = useForm();

  const [readOnlyTitle, setReadOnlyTitle] = useState(true);
  const [readOnlyDifficulty, setReadOnlyDifficulty] = useState(true);

  const switchReadOnlyTitle = () => {
    setReadOnlyTitle(!readOnlyTitle);
  };

  const switchReadOnlyDifficulty = () => {
    setReadOnlyDifficulty(!readOnlyDifficulty);
  };

  const handleClickOnCheckTitle = () => {
    editGoal({ title: title }, actual.id);
    switchReadOnlyTitle();
  };

  const handleClickOnCheckDifficulty = () => {
    editGoal({ difficulty: difficulty }, actual.id);
    switchReadOnlyDifficulty();
  };

  return (
    <Container>
      <Content>
      <GoalTitle className="goalTitle">
        <ChangeInfo onClick={switchReadOnlyTitle}>
          <Input
            register={register}
            name="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            label={title}
            readOnly={readOnlyTitle}
          />
        </ChangeInfo>
        {!readOnlyTitle && (
          <SendInfo onClick={handleClickOnCheckTitle}>
            Enviar
            <IoMdSend style={{ marginLeft: "0.5rem" }} />
          </SendInfo>
        )}
      </GoalTitle>
      <GoalDifficulty className="goalDifficulty">
        <ChangeInfo onClick={switchReadOnlyDifficulty}>
          <Input
            register={register}
            name="difficulty"
            label={difficulty}
            value={difficulty}
            placeholder="enviar"
            onChange={(e) => setDifficulty(e.target.value)}
          />
        </ChangeInfo>
        {!readOnlyDifficulty && (
          <SendInfo onClick={handleClickOnCheckDifficulty}>
            Enviar <IoMdSend style={{ marginLeft: "0.5rem" }} />
          </SendInfo>
        )}
      </GoalDifficulty>
      </Content>

      <Buttons>
      {!userIsInGroup && (
        <Button 
          classNameProp="checkHabit cardHabit"
          onClick={() => progressInGoal(actual.how_much_achieved, actual.id)}
        >
          Check
        </Button>
      )}

      {!userIsInGroup && (
          <Button 
          classNameProp="deleteHabit cardHabit"
          onClick={() => deleteGoal(actual.id)}
        >
          Excluir
        </Button>
      )}
      </Buttons>

    </Container>
  );
};

export default Goal;
