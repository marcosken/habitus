import styled from "styled-components";

export const Container = styled.div `
  margin: 1rem 0;
  width: 100vw;
  height: 120;
  box-shadow: 0 0 10px 1px var(--mono1);
  padding: 1rem;
  max-width: 425px;
`;
export const Content = styled.h2 `
  display: flex;
`;
export const ChangeInfo = styled.div `
  width: 90%;
  border: none;
  display: flex;
  color: var(--mono4);
  font-size: 1.2rem;
`;
export const SendInfo = styled.button `
  border: none;
  display: flex;
  color: var(--mono4);
  font-size: 1.2rem;
  margin-bottom: 1rem;
`;
export const GoalTitle = styled.div ``;
export const GoalDifficulty = styled.div ``;

export const Buttons = styled.div `
  width: 100%;
  justify-content: space-around;
`