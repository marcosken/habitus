import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Goal from "../goal";
import Input from "../input";
import { Container, ContainerSearch, ContainerCards } from "./styles";
import { BsSearch } from "react-icons/bs";

const GoalsList = ({ currentGroup, userIsInGroup }) => {
  const [searchResults, setSearchResults] = useState("");

  const { register } = useForm();

  const [userInput, setUserInput] = useState("");

  useEffect(() => {
    if (userInput) {
      setSearchResults(
        currentGroup.goals.filter((el) =>
          el.title.toLowerCase().includes(userInput.toLowerCase())
        )
      );
    } else {
      setSearchResults("");
    }
  }, [userInput]);

  return (
    <Container>
      <ContainerSearch>
        <Input
          register={register}
          name="userInput"
          value={userInput}
          onChange={(e) => setUserInput(e.target.value)}
          label="Pesquisar meta"
          icon={BsSearch}
        />
      </ContainerSearch>
      {searchResults ? (
        searchResults.map((el, index) => <Goal key={index} actual={el} />)
      ) : (
        <ContainerCards>
          {currentGroup.goals?.map((el, index) => (
            <Goal key={index} actual={el} currentGroup={currentGroup} />
          ))}
        </ContainerCards>
      )}
    </Container>
  );
};

export default GoalsList;
