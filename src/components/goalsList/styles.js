import styled from "styled-components";

export const Container = styled.section `
  width: 100%;
  padding: 0 0.5rem;
`;

export const ContainerSearch = styled.section `
    width: 290px;
    margin: 1rem auto;

    @media (min-width: 768px) {
        width: 400px;
    }
`;

export const ContainerCards = styled.section ``;