import { useState } from "react";
import Activity from "../activity";
import { Button } from "../button";
import Goal from "../goal";
import Modal from "react-modal";
import {
  BtnCloseModal,
  Figure,
  Section,
  TitleGoal,
  TitleActivity,
  Image,
  Conainer,
  Name,
  Description,
  Category,
  ConainerAbout,
  ContainerNavigationButtons,
  ContainerCards,
  ContainerCardsHeader,
} from "./styles";
import FormCreateGoal from "../../components/formCreateGoal";
import FormCreateActivity from "../../components/formCreateActivity";
import newGoal from "../../assets/img/newHabit.svg";
import newActivity from "../../assets/img/newGroup.svg";
import GoalsList from "../goalsList";
import ActivityList from "../activityList";
import { useMyGroups } from "../../providers/myGroups";
import { useGroups } from "../../providers/groups";
import FormEditGroup from "../formEditGroup";

const GroupDetails = ({ actual, userIsInGroup = true }) => {
  const [displayCardList, setDisplayCardList] = useState(true);
  const [activitySelected, setActivitySelected] = useState(true);
  const [goalSelected, setGoalSelected] = useState(false);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [isGoal, setIsGoal] = useState(false);
  const [editModalIsOpen, setEditModalIsOpen] = useState(false);


  const toggleEditModal = () => {
    setEditModalIsOpen(!editModalIsOpen);
  }

  const { subscribeToGroup } = useMyGroups();

  const customStyles = {
    content: {
      background: "var(--mono3)",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
  };

  const toggleModal = (type) => {
    if (type === "goal") {
      setIsGoal(true);
    } else {
      setIsGoal(false);
    }
    setModalIsOpen(!modalIsOpen);
  };

  const handleClickActivity = () => {
    setDisplayCardList(true);
    setActivitySelected(true);
    setGoalSelected(false);
  };
  const handleClickGoal = () => {
    setDisplayCardList(false);
    setActivitySelected(false);
    setGoalSelected(true);
  };

  return (
    <Conainer>
      <ConainerAbout>
        <Name>{actual?.name}</Name>
        <Description>
          <span>Descrição: </span>
          {actual?.description}
        </Description>
        <Category>
          <span>Categoria: </span>
          {actual?.category}
        </Category>
        
        <Button className="addBtnModal" style={{width: "100px", borderRadius: "1.5rem"}}onClick={toggleEditModal}>
          Editar
        </Button>
        
        <Modal
        isOpen={editModalIsOpen}
        ariaHideApp={false}
        onRequestClose={toggleEditModal}
        style={customStyles}
        >
          <FormEditGroup actual={actual} toggleEditModal={toggleEditModal}/>
        </Modal>
        {!userIsInGroup && (
          <Button
            classNameProp="check"
            onClick={() => subscribeToGroup(actual)}
          >
            Participar do grupo
          </Button>
        )}
      </ConainerAbout>
      <ContainerNavigationButtons>
        {activitySelected ? (
          <Button
            selected
            classNameProp="activityAndGoalButton"
            activitySelected={activitySelected}
            onClick={handleClickActivity}
          >
            ATIVIDADES
          </Button>
        ) : (
          <Button
            classNameProp="activityAndGoalButton"
            activitySelected={activitySelected}
            onClick={handleClickActivity}
          >
            ATIVIDADES
          </Button>
        )}

        {goalSelected ? (
          <Button
            classNameProp="activityAndGoalButton"
            selected
            goalSelected={goalSelected}
            onClick={handleClickGoal}
          >
            METAS
          </Button>
        ) : (
          <Button
            classNameProp="activityAndGoalButton"
            goalSelected={goalSelected}
            onClick={handleClickGoal}
          >
            METAS
          </Button>
        )}
      </ContainerNavigationButtons>

      <div>
        {displayCardList ? (
          <ContainerCards style={{ display: "flex", flexFlow: "row wrap" }}>
            <ContainerCardsHeader>
              <h2>Atividades</h2>
              <Button
                className="addActivity"
                onClick={() => toggleModal("activity")}
              >
                NOVA ATIVIDADE
              </Button>
            </ContainerCardsHeader>
            <ActivityList currentGroup={actual} />
          </ContainerCards>
        ) : (
          <ContainerCards style={{ display: "flex", flexFlow: "row wrap" }}>
            <ContainerCardsHeader>
              <h2>Metas</h2>
              <Button className="addGoal" onClick={() => toggleModal("goal")}>
                NOVA META
              </Button>
            </ContainerCardsHeader>
            <GoalsList userIsInGroup={userIsInGroup} currentGroup={actual} />
          </ContainerCards>
        )}
      </div>

      <Modal
        isOpen={modalIsOpen}
        ariaHideApp={false}
        onRequestClose={toggleModal}
        style={customStyles}
      >
        <BtnCloseModal
          style={{ position: "absolute", right: "0.5rem", top: "0.2rem" }}
          onClick={toggleModal}
        >
          X
        </BtnCloseModal>
        <Section>
          <div>
            {isGoal ? (
              <TitleGoal>Nova meta</TitleGoal>
            ) : (
              <TitleActivity>Nova atividade</TitleActivity>
            )}
            <Figure>
              {isGoal ? (
                <Image src={newGoal} alt="newGoal" />
              ) : (
                <Image src={newActivity} alt="newActivity" />
              )}
            </Figure>
          </div>
          {isGoal ? (
            <FormCreateGoal toggleModal={toggleModal} groupID={actual.id} />
          ) : (
            <FormCreateActivity toggleModal={toggleModal} groupID={actual.id} />
          )}
        </Section>
      </Modal>
    </Conainer>
  );
};

export default GroupDetails;
