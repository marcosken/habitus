import styled from "styled-components";

export const Conainer = styled.div`
  font-family: var(--font3);
  @media(min-width: 1024px){
    padding: 20px;
  }
`;
export const ConainerAbout = styled.section`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  flex-wrap: wrap;
  padding: 0.5rem;
  margin-top: 1rem;
  margin-bottom: 1rem;
`;

export const Name = styled.h1`
  width: 100%;
  text-align: center;
  font-size: 1.5rem;
  text-transform: uppercase;
`;

export const ContainerNavigationButtons = styled.div`
  display: flex;
  justify-content: space-around;
  margin-bottom: 1rem;

  @media (min-width: 768px) {
    display: block;
    text-align: center;
    margin-bottom: 1rem;
    :first-of-type button {
      margin-right: 2rem;
    }
  }
`;

export const ContainerCards = styled.section`
  display: flex;
  justify-content: center;
`;

export const ContainerCardsHeader = styled.section`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 100%;
  margin-bottom: 1rem;
  h2 {
    font-size: 1.5rem;
    text-transform: uppercase;
  }
`;

export const Description = styled.p`
  width: 100%;
  font-size: 1rem;
  font-family: var(--font1);
  margin-top: 1rem;

  span {
    text-transform: uppercase;
    font-family: var(--font3);
  }
`;

export const Category = styled.p`
  width: 100%;
  font-size: 1rem;
  font-family: var(--font1);
  margin-top: 1rem;

  span {
    text-transform: uppercase;
    font-family: var(--font3);
  }
`;

export const BtnCloseModal = styled.button`
  border: none;
  background: transparent;
  color: var(--mono8);
  font-size: 2rem;
  font-family: var(--font3);
  cursor: pointer;
  :hover {
    font-weight: bold;
  }
`;

export const TitleGoal = styled.h2`
  font-family: var(--font3);
  color: var(--mono4);
  text-align: center;
  font-size: 2.5rem;
  margin: 3rem 0;
  @media (min-width: 1024px) {
    margin: 2rem 0;
    font-size: 4rem;
  }
`;

export const TitleActivity = styled(TitleGoal)`
  margin: 3rem 1rem;
`;

export const Section = styled.section`
  width: 100%;

  @media (min-width: 768px) {
    display: flex;
    height: 100%;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  @media (min-width: 1024px) {
    flex-direction: row;
    justify-content: space-around;
  }
`;

export const Figure = styled.figure`
  display: none;

  @media (min-width: 1024px) {
    display: block;
    width: 80%;
    margin: 0 auto;
  }
`;

export const Image = styled.img`
  width: 100%;
`;
