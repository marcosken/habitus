import { useEffect } from "react";
import { useHabits } from "../../providers/habits";
import { useUser } from "../../providers/user";
import { Button } from "./../button";
import {
  Container,
  Title,
  Infos,
  ContainerButtons,
  ProgressBar,
  Progress,
  Percentage,
} from "./styles";

const HabitsCard = ({ actual }) => {
  const { deleteHabit, progressInHabit } = useHabits();
  const { accessToken } = useUser();
  useEffect(() => {}, [actual]);

  return (
    <Container achieved={actual.achieved}>
      <Title>{actual.title}</Title>
      <Infos>
        <span>CATEGORIA: </span>
        {actual.category}
      </Infos>
      <Infos>
        <span>DIFICULDADE: </span>
        {actual.difficulty}
      </Infos>
      <Infos>
        <span>frequência: </span>Diária
      </Infos>
      <ProgressBar>
        <Progress widthProgress={actual.how_much_achieved}>
          <Percentage>{actual.how_much_achieved}%</Percentage>
        </Progress>
      </ProgressBar>
      <ContainerButtons>
        <Button
          disabled={actual.achieved}
          classNameProp="checkHabit cardHabit"
          onClick={() =>
            progressInHabit(actual.id, actual.how_much_achieved, accessToken)
          }
        >
          CHECK
        </Button>
        <Button
          classNameProp="deleteHabit cardHabit"
          onClick={() => deleteHabit(actual.id, accessToken)}
        >
          EXCLUIR
        </Button>
      </ContainerButtons>
    </Container>
  );
};

export default HabitsCard;
