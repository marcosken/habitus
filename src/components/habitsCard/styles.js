import styled from "styled-components";

export const Container = styled.div`
  width: 250px;
  height: 300px;
  background-color: var(--mono8);
  box-shadow: 0 0 10px 2px
    ${(props) => (props.achieved ? "rgba(48,199,76,0.75)" : "var(--mono5)")};
  border-radius: 1rem;
  font-size: 1rem;
  color: var(--mono4);
  margin: 5px;
  font-family: var(--font1);
  padding: 1.2rem 0.8rem;
  @media (min-width: 1024px) {
  }
`;
export const Title = styled.h2`
  font-weight: bold;
  text-align: center;
  width: 100%;
  font-size: 1.4rem;
  text-transform: uppercase;
  margin: 0.5rem 0 1rem;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  span {
    text-transform: uppercase;
  }
  @media (min-width: 1024px) {
    font-size: 1.2rem;
  }
`;

export const Infos = styled.h2`
  width: 100%;
  height: 1rem;
  margin: 0.5rem 0;
  padding: 0 0.5rem;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 1rem;
  span {
    text-transform: uppercase;
    font-weight: bold;
    font-size: 0.9rem;
  }
`;

export const ContainerButtons = styled.div`
  width: 90%;
  margin: 0 auto;
  display: flex;
  gap: 0.5rem;
  justify-content: space-around;
  flex-wrap: wrap;
  span {
    text-transform: uppercase;
    font-weight: bold;
  }
`;

export const ProgressBar = styled.div`
  width: var(--widthProgressBarMobile);
  height: 1rem;
  margin: 1.5rem auto;
  display: flex;
  align-items: center;
  background-color: var(--mono4);
  border: 0.2rem solid var(--mono4);
  /* position: relative; */

  @media (min-width: 1024px) {
    width: var(--widthProgressBarDesktop);
    height: 1.25rem;
  }
`;

export const Progress = styled.div`
  width: ${(props) => `${props.widthProgress}%`};
  height: 0.8rem;
  background-color: var(--mono2);

  @media (min-width: 1024px) {
    width: ${(props) => `${props.widthProgress}%`};
    height: 1rem;
  }
`;

export const Percentage = styled.h2`
  color: var(--mono8);
  width: var(--widthProgressBarMobile);
  /* position: absolute; */
  text-align: center;

  @media (min-width: 1024px) {
    width: var(--widthProgressBarDesktop);
  }
`;
