import { useEffect, useState } from "react";
import { useHabits } from "../../providers/habits";
import HabitsCard from "../habitsCard";
import { Container, Title } from "./styles";
import loading from "../../assets/img/loading.svg";

const HabitsList = () => {
  const { habits, isLoading, setIsLoading, loadHabits } = useHabits();
  const [accessToken, setAccessToken] = useState("");

  useEffect(() => {
    setAccessToken(
      (oldState) => (oldState = localStorage.getItem("authToken"))
    );
    if (accessToken !== "") {
      loadHabits(accessToken).then((res) => setIsLoading(false));
    }
  }, [accessToken, loadHabits, setIsLoading]);

  return (
    <Container countHabits={habits.length}>
      {isLoading && <img src={loading} alt="loading" />}

      {!isLoading && habits.length === 0 ? (
        <Title>Você não tem hábitos cadastrados!</Title>
      ) : (
        habits?.map((el) => <HabitsCard key={el.id} actual={el} />)
      )}
    </Container>
  );
};

export default HabitsList;
