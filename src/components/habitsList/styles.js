import styled from "styled-components";

export const Container = styled.div`
  width: 90%;
  display: flex;
  justify-content: center;
  position: relative;
  top: ${(prop) => (prop.countHabits === 0 ? "2.5rem" : "0")};
  flex-wrap: wrap;
  margin: 0 auto;
  padding: 2rem 0;
`;

export const Title = styled.h2`
  font-family: var(--font3);
  font-size: 2rem;
  color: var(--mono5);
  text-align: center;

  @media (min-width: 1024px) {
    font-size: 2rem;
  }
`;
