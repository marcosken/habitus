import {
  Background,
  FigureDesktop,
  FigureMobile,
  LogoDesktop,
  LogoMobile,
  MenuMobile,
  MenuDesktop,
  MenuMobileOptions,
  OptionsMobile,
  MenuHamburger,
  MenuDesktopOptions,
  OptionsDesktop,
  IconDesktop,
} from "./styles";
import logoMarcaDesktop from "../../assets/img/logo-desktop.svg";
import logoTipoMobile from "../../assets/img/logo-mobile.svg";
import { Link, useHistory } from "react-router-dom";
import { useState } from "react";
import { FaUserCircle } from "react-icons/fa";
import { useUser } from "../../providers/user";

const Header = () => {
  const { isLogged } = useUser();
  const [mobileIsOpen, setMobileIsOpen] = useState(false);
  const [desktopIsOpen, setDesktopIsOpen] = useState(false);
  const history = useHistory();

  const handleLogout = () => {
    localStorage.clear();
    history.push("/login");
    return window.location.reload();
  };

  window.addEventListener("scroll", function (event) {
    setTimeout(function () {
      setMobileIsOpen(false);
      setDesktopIsOpen(false);
    }, 15);
  });

  return (
    <Background>
      <FigureMobile>
        <Link to="/">
          <LogoMobile src={logoTipoMobile} alt="logoTipo" />
        </Link>
      </FigureMobile>
      <FigureDesktop>
        <Link to="/">
          <LogoDesktop src={logoMarcaDesktop} alt="logoMarca" />
        </Link>
      </FigureDesktop>

      {isLogged && (
        <>
          <MenuMobile>
            <MenuHamburger onClick={() => setMobileIsOpen(!mobileIsOpen)}>
              <div />
              <div />
              <div />
            </MenuHamburger>
            {mobileIsOpen && (
              <MenuMobileOptions>
                <OptionsMobile
                  onClick={() => {
                    setMobileIsOpen(!mobileIsOpen);
                    history.push("/dashboard");
                  }}
                >
                  Hábitos
                </OptionsMobile>
                <OptionsMobile
                  onClick={() => {
                    setMobileIsOpen(!mobileIsOpen);
                    history.push("/groups");
                  }}
                >
                  Grupos
                </OptionsMobile>
                <OptionsMobile
                  onClick={() => {
                    setMobileIsOpen(!mobileIsOpen);
                    history.push("/userData");
                  }}
                >
                  Perfil
                </OptionsMobile>
                <OptionsMobile
                  onClick={() => {
                    setMobileIsOpen(!mobileIsOpen);
                    handleLogout();
                  }}
                >
                  Sair
                </OptionsMobile>
              </MenuMobileOptions>
            )}
          </MenuMobile>

          <MenuDesktop>
            <OptionsDesktop
              onClick={() => {
                setDesktopIsOpen(false);
                history.push("/dashboard");
              }}
            >
              Hábitos
            </OptionsDesktop>
            <OptionsDesktop
              onClick={() => {
                setDesktopIsOpen(false);
                history.push("/groups");
              }}
            >
              Grupos
            </OptionsDesktop>
            <IconDesktop>
              <FaUserCircle onClick={() => setDesktopIsOpen(!desktopIsOpen)} />
            </IconDesktop>
            {desktopIsOpen && (
              <MenuDesktopOptions>
                <h2
                  onClick={() => {
                    setDesktopIsOpen(!desktopIsOpen);
                    history.push("/userData");
                  }}
                >
                  Perfil
                </h2>
                <h2
                  onClick={() => {
                    setDesktopIsOpen(!desktopIsOpen);
                    handleLogout();
                  }}
                >
                  Sair
                </h2>
              </MenuDesktopOptions>
            )}
          </MenuDesktop>
        </>
      )}
    </Background>
  );
};

export default Header;
