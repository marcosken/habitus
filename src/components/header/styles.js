import styled from "styled-components";

export const Background = styled.div`
  width: 100vw;
  background-color: var(--mono2);
  height: var(--heightHeaderMobile);
  box-shadow: 1px 0 5px 5px #cccccc;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (min-width: 768px) {
    height: var(--heightHeaderDesktop);
  }
`;

export const FigureMobile = styled.figure`
  margin-left: 1rem;
  max-height: var(--heightHeaderDesktop);
  width: 4rem;

  @media (min-width: 768px) {
    display: none;
  }
`;

export const LogoMobile = styled.img`
  width: 100%;
  @media (min-width: 768px) {
    display: none;
  }
`;

export const FigureDesktop = styled.figure`
  display: none;

  @media (min-width: 768px) {
    display: flex;
    width: 248px;
    margin-left: 1rem;
  }
`;

export const LogoDesktop = styled.img`
  display: none;

  :hover {
    transform: scale(1.05);
  }
  @media (min-width: 768px) {
    width: 100%;
    display: flex;
  }
`;

export const MenuMobile = styled.div`
  margin-right: 1rem;

  @media (min-width: 768px) {
    display: none;
  }
`;

export const MenuHamburger = styled.div`
  width: 2rem;
  height: 2rem;
  cursor: pointer;

  div {
    width: 100%;
    height: 20%;
    margin: 10% 0;
    background-color: var(--mono4);
  }

  :active {
    transform: rotate(200grad);
    transition: transform 0.15s;
  }
`;

export const MenuMobileOptions = styled.div`
  position: fixed;
  top: var(--heightHeaderMobile);
  right: 0;
  height: calc(100vh - var(--heightHeaderMobile));
  width: 50%;
  background-color: var(--mono3);
  font-size: 1.8rem;
  font-family: var(--font3);
  color: var(--mono4);
  z-index: 1;
`;
export const OptionsMobile = styled.h2`
  text-align: end;
  margin: 1rem 1rem 1rem 2rem;
  cursor: pointer;

  :hover {
    filter: brightness(130%);
  }
`;

export const MenuDesktop = styled.div`
  display: none;

  @media (min-width: 768px) {
    width: 290px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-right: 2rem;
  }
`;
export const OptionsDesktop = styled.h2`
  font-size: 1.7rem;
  font-family: var(--font3);
  color: var(--mono4);
  cursor: pointer;

  :hover {
    filter: brightness(130%);
  }
`;

export const IconDesktop = styled.h1`
  font-size: 3rem;
  color: var(--mono4);
  cursor: pointer;

  :hover {
    filter: brightness(130%);
  }
`;

export const MenuDesktopOptions = styled.div`
  position: fixed;
  top: var(--heightHeaderDesktop);
  right: 1rem;
  font-family: var(--font3);
  color: var(--mono4);
  font-weight: bold;
  font-size: 1.5rem;
  z-index: 1;
  background: var(--mono3);
  width: 8rem;

  h2 {
    text-align: end;
    padding: 0 1rem;
    margin: 1rem 0;
    cursor: pointer;

    :hover {
      filter: brightness(130%);
    }
  }
`;
