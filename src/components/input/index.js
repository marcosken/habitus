import { Container, ContainerInput } from "./styles";

const Input = ({ label, icon: Icon, error, register, name, ...rest }) => {
  return (
    <Container>
      <p>
        {label} {!!error && <span> - {error}</span>}
      </p>
      <ContainerInput isInvalid={!!error}>
        {Icon && <Icon size={24} />}
        <input {...register(name)} {...rest} />
      </ContainerInput>
    </Container>
  );
};

export default Input;
