import styled, { css } from "styled-components";

export const Container = styled.div`
  text-align: left;
  font-family: var(--font2);
  p {
    margin-bottom: 0.2rem;
    font-weight: bold;
    color: var(--mono4);
  }
  span {
    color: var(--mono6);
    font-weight: normal;
  }
`;

export const ContainerInput = styled.div`
  border-radius: 0.2rem;
  border: 1px solid var(--mono4);
  padding: 0.4rem 0.8rem;
  display: flex;
  background: var(--mono8);

  input {
    border: none;
    outline: none;
    flex: 1;
    width: 100%;
    font-size: 1rem;
    padding: 0;
    color: var(--mono1);

    &::placeholder {
      color: var(--mono5);
      font-weight: bold;
    }
  }

  svg {
    margin-right: 1rem;
    color: var(--mono4);
  }
  ${(props) =>
    props.isInvalid &&
    css`
      border-color: var(--mono6);
      svg {
        color: var(--mono6);
      }
    `}
`;
