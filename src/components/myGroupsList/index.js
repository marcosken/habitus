import GroupCard from "../GroupCard";
import { useEffect } from "react";
import { useMyGroups } from "../../providers/myGroups";
import loading from "./../../assets/img/loading.svg";
import { Container, ContainerCards, Content } from "./styles";
import { useUser } from "../../providers/user";

const MyGroupsList = () => {
  const { isLoading, setIsloading, myGroups, loadMyGroups } = useMyGroups();
  const { accessToken } = useUser();

  useEffect(() => {
    loadMyGroups(accessToken);
  }, [accessToken, loadMyGroups, setIsloading]);

  return (
    <Container>
      <ContainerCards>
        {isLoading && <img src={loading} alt="loading" />}
        {myGroups?.length !== 0 ? (
          myGroups?.map((group) => (
            <GroupCard key={group.id} group={group} noButton />
          ))
        ) : (
          <>
            <Content>
              Você não está inscrito em nenhum grupo!
              <br />
              Participe de algum dos grupos existentes logo abaixo ou então crie
              novos grupos.
            </Content>
          </>
        )}
      </ContainerCards>
    </Container>
  );
};

export default MyGroupsList;
