import styled from "styled-components";

export const Container = styled.div`
  width: 90%;
  margin: 0 auto 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1rem 0;
  @media (min-width: 1024px) {
    margin-bottom: 2rem;
  }
`;

export const ContainerCards = styled.div`
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;

  @media (min-width: 768px) {
    width: 650px;
  }
`;
export const Content = styled.span`
  font-family: var(--font3);
  font-size: 1.3rem;
  color: var(--mono5);
  text-align: center;
  display: block;
  margin: auto 0;
  padding: 1.5rem 0;

  @media (min-width: 1024px) {
    font-size: 2rem;
  }
`;
