import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useGroups } from "../../providers/groups";
import Input from "../input";
import { BsSearch } from "react-icons/bs";
import { Container } from "./styles";

const SearchGroup = () => {
  const { groups, searchGroups, setSearchResults } = useGroups();

  const { register } = useForm();

  const [userInput, setUserInput] = useState("");

  const onChangeSearch = (e) => {
    setUserInput(e.target.value);
    searchGroups(userInput);
    if (!userInput) {
      setSearchResults([]);
    }
  };

  useEffect(() => {
    if (!userInput) {
      setSearchResults([]);
    }
  }, [userInput])

  return (
    <Container>
      <Input
        className="search"
        register={register}
        icon={BsSearch}
        name="userInput"
        value={userInput}
        onChange={(e) => onChangeSearch(e)}
        title="Pesquisar grupo"
        placeholder="Pesquisar grupo"
      />
    </Container>
  );
};

export default SearchGroup;
