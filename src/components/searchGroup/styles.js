import styled from "styled-components";

export const Container = styled.div`
  margin-top: 1rem;
  div {
    border-radius: 50px;
  }
  @media (min-width: 768px) {
    margin-top: 0;
  }
`;
