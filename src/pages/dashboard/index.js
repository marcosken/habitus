import HabitsList from "../../components/habitsList";
import Modal from "react-modal";
import FormCreateHabit from "./../../components/formCreateHabit";
import { useState } from "react";
import { Button } from "./../../components/button";
import {
  Container,
  BubbleOne,
  BubbleTwo,
  BubbleThree,
  ArrowIcon,
  Title,
  Content,
  BtnCloseModal,
  TitleHabit,
  Section,
  Figure,
  Image,
} from "./styles";
import newHabit from "../../assets/img/newHabit.svg";
import { useUser } from "../../providers/user";
import { Redirect } from "react-router-dom";

const Dashboard = () => {
  const { isLogged } = useUser();

  const [modalIsOpen, setModalIsOpen] = useState(false);

  const toggleModal = () => {
    setModalIsOpen(!modalIsOpen);
  };

  const customStyles = {
    content: {
      background: "var(--mono3)",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      borderRadius: "1rem",
      padding: "10px",
    },
  };

  if (!isLogged) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>
      <Title>Hábitos</Title>
      <Content>
        <ArrowIcon />
        <Button classNameProp="newHabit" onClick={toggleModal}>
          Novo hábito
        </Button>
      </Content>

      <Modal
        isOpen={modalIsOpen}
        ariaHideApp={false}
        onRequestClose={toggleModal}
        style={customStyles}
      >
        <BtnCloseModal
          style={{ position: "absolute", right: "0.5rem", top: "0.2rem" }}
          onClick={toggleModal}
        >
          X
        </BtnCloseModal>
        <Section>
          <div>
            <TitleHabit>Novo hábito</TitleHabit>
            <Figure>
              <Image src={newHabit} alt="newHabit" />
            </Figure>
          </div>
          <FormCreateHabit toggleModal={toggleModal} />
        </Section>
      </Modal>

      <HabitsList />

      <BubbleOne />
      <BubbleTwo />
      <BubbleThree />
    </Container>
  );
};

export default Dashboard;
