import styled from "styled-components";
import IconArrow from "./../../assets/img/arrow-return-right.svg";

export const Container = styled.main`
  width: 100vw;
  height: calc(88vh - var(--heightHeaderMobile));

  @media (min-width: 1024px) {
    height: calc(88vh - var(--heightHeaderDesktop));
  }
`;

export const Title = styled.h1`
  font-family: var(--font1);
  font-weight: bold;
  margin: 1rem 0 0 1rem;
  color: var(--mono4);
  font-size: 40px;

  @media (min-width: 1024px) {
    margin: 2rem 0 0 2rem;
    font-size: 3rem;
  }
`;

export const Content = styled.div`
  display: flex;
  align-items: center;
  margin-left: 2.5rem;

  @media (min-width: 1024px) {
    margin-left: 3.6rem;
  }
`;

export const ArrowIcon = styled.div`
  background: url(${IconArrow}) no-repeat center;
  background-size: contain;
  width: 36px;
  height: 32px;
  margin-bottom: 0.5rem;

  @media (min-width: 1024px) {
    width: 43px;
    height: 39x;
    margin-bottom: 1rem;
  }
`;

export const BubbleOne = styled.div`
  width: 90px;
  height: 90px;
  border-radius: 50%;
  background-color: var(--mono2);
  position: fixed;
  left: calc(100% - 40px);
  top: 20%;
  z-index: -1;

  @media (min-width: 1024px) {
    width: 180px;
    height: 180px;
    left: calc(100% - 90px);
    top: 20%;
  }
`;

export const BubbleTwo = styled.div`
  width: 160px;
  height: 160px;
  border-radius: 50%;
  background-color: var(--mono2);
  position: fixed;
  left: calc(0% - 80px);
  top: calc(50% - 80px);
  z-index: -1;

  @media (min-width: 1024px) {
    width: 400px;
    height: 400px;
    left: calc(0% - 270px);
    top: calc(50% - 100px);
  }
`;

export const BubbleThree = styled.div`
  width: 148px;
  height: 148px;
  border-radius: 50%;
  background-color: var(--mono2);
  position: fixed;
  left: calc(100% - 74px);
  top: 80%;
  z-index: -1;

  @media (min-width: 1024px) {
    width: 320px;
    height: 320px;
    left: calc(100% - 200px);
    top: calc(100% - 200px);
  }
`;

export const BtnCloseModal = styled.button`
  border: none;
  background: transparent;
  color: var(--mono8);
  font-size: 2rem;
  font-family: var(--font3);
  cursor: pointer;
  :hover {
    font-weight: bold;
  }
`;

export const TitleHabit = styled.h2`
  font-family: var(--font3);
  color: var(--mono4);
  text-align: center;
  font-size: 2.5rem;
  margin: 2rem 0 3rem;
  @media (min-width: 1024px) {
    margin: 2rem 0;
    font-size: 4rem;
  }
`;

export const Section = styled.section`
  width: 100%;

  @media (min-width: 768px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
  }
  @media (min-width: 1024px) {
    flex-direction: row;
  }
`;

export const Figure = styled.figure`
  display: none;

  @media (min-width: 1024px) {
    display: block;
    width: 80%;
    margin: 0 auto;
  }
`;

export const Image = styled.img`
  width: 100%;
`;
