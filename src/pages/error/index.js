import { Button } from "../../components/button";
import { useHistory } from "react-router";

import { Container, Background, Ball } from "./styles";
import logo from "../../assets/img/error.png";
import ball from "../../assets/img/ball_s_blue.png";

const Error = () => {
  const history = useHistory();

  return (
    <Container>
      <Background>
        <figure>
          <img src={logo} alt="logoImage" />
        </figure>
      </Background>
      <Ball>
        <figure>
          <img src={ball} alt="ballImage" />
        </figure>
      </Ball>
      <h1>ERRO 404</h1>
      <p>Desculpe, a página que você está procurando não existe.</p>
      <Button onClick={() => history.push("/")}>HOME</Button>
    </Container>
  );
};

export default Error;
