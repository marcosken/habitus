import styled from "styled-components";

export const Container = styled.div`
  position: absolute;
  width: 80%;
  height: 60%;
  right: 10vw;
  top: 30%;

  h1 {
    color: var(--mono4);
    font-size: 90px;
    text-align: center;
    font-family: var(--font3);
    margin: 100px 50px 50px;
    text-shadow: 2px 2px 6px #ffffff;
  }

  p {
    color: var(--mono1);
    font-family: var(--font3);
    font-size: 25px;
    line-height: 1.3;
    margin-left: 30px;
    text-shadow: 1px 1px 3px #ffffff;
  }

  button {
    background-color: var(--mono8);
    color: var(--mono1);
    width: 150px;
    height: 50px;
    border-radius: 40px;
    border: 5px solid var(--mono1);
    font-size: 20px;
    font-family: var(--font3);
    position: relative;
    bottom: -5vh;
    left: 20vw;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 6rem;
    :hover {
      cursor: pointer;
      background-color: var(--mono9);
      box-shadow: 10px 10px 28px 0px rgba(0, 0, 0, 0.75);
    }
  }

  @media (min-width: 600px) {
    width: 30%;
    min-width: 400px;

    button {
      left: 40%;
      bottom: -10vh;
    }
  }

  @media (min-width: 1024px) {
    width: 30%;

    button {
      left: 55%;
      bottom: -10vh;
    }
  }
`;

export const Background = styled.div`
  width: 90vw;
  position: absolute;
  z-index: -1;
  margin: 6rem auto;
  display: flex;
  justify-content: center;
  right: -25px;
  top: -200px;
  opacity: 0.5;
  img {
    max-width: 800px;
  }
  figure {
    img {
      width: 100%;
    }
  }

  @media (min-width: 768px) {
    opacity: 0.7;
    figure {
      width: 30rem;
      position: absolute;
      z-index: -1;
      margin: 6rem 0;
      display: flex;
      justify-content: center;
      left: 15%;
      top: 15%;
    }
  }

  @media (min-width: 1024px) {
    figure {
      width: 35rem;
      position: absolute;
      z-index: -1;
      display: flex;
      justify-content: center;
      left: 15%;
      top: 25%;
      opacity: 1;
    }
  }
`;

export const Ball = styled.div`
  width: 20vw;
  position: absolute;
  z-index: -1;
  margin: 6rem auto;
  display: flex;
  justify-content: center;
  right: 0;
  top: 20vh;

  figure {
    img {
      width: 100%;
    }
  }
`;
