import MyGroupsList from "../../components/myGroupsList";
import { useUser } from "../../providers/user";
import { Redirect } from "react-router-dom";
import FormCreateGroup from "./../../components/formCreateGroup";
import GroupsList from "./../../components/GroupsList";
import Modal from "react-modal";
import { useState } from "react";
import {
  Container,
  BtnCloseModal,
  Section,
  TitleNewGroup,
  Figure,
  Image,
  Title,
  ContentOne,
  ArrowIcon,
  TitleTwo,
  ContentTwo,
  ReturnTopIcon,
  MyGroupsContainer,
  MyGroupsContent,
  BannerDesktop,
  ImgBannerDesktop,
  MsgBanner,
  BubbleOne,
  BubbleTwo,
  BubbleThree,
} from "./styles";
import newGroup from "./../../assets/img/newGroup.svg";
import { useMyGroups } from "../../providers/myGroups";
import { Button } from "../../components/button";
import SearchGroup from "../../components/searchGroup";
import { IoIosArrowDropupCircle } from "react-icons/io";

const Groups = () => {
  const { isLoading, myGroups, MAX_NUMBER_OF_GROUPS } = useMyGroups();
  const { isLogged } = useUser();
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const toggleModal = () => {
    setModalIsOpen(!modalIsOpen);
  };

  const customStyles = {
    content: {
      background: "var(--mono3)",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      borderRadius: "1rem",
      padding: "10px",
    },
  };

  if (!isLogged) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>
      <ContentOne>
        <Title>Grupos</Title>
        <ArrowIcon />
      </ContentOne>

      <Button
        classNameProp="newHabit newGoup"
        onClick={toggleModal}
        disabled={myGroups.length === MAX_NUMBER_OF_GROUPS}
      >
        Novo grupo
      </Button>
      <Modal
        isOpen={modalIsOpen}
        ariaHideApp={false}
        onRequestClose={toggleModal}
        style={customStyles}
      >
        <BtnCloseModal
          style={{ position: "absolute", right: "0.5rem", top: "0.2rem" }}
          onClick={toggleModal}
        >
          X
        </BtnCloseModal>
        <Section>
          <div>
            <TitleNewGroup>Novo grupo</TitleNewGroup>
            <Figure>
              <Image src={newGroup} alt="newGroup" />
            </Figure>
          </div>
          <FormCreateGroup toggleModal={toggleModal} />
        </Section>
      </Modal>

      <MyGroupsContainer>
        <BannerDesktop>
          <ImgBannerDesktop />
          <MsgBanner>
            Aqui você pode se conectar com outros usuários que compartilham os
            mesmos objetivos!
          </MsgBanner>
        </BannerDesktop>
        <MyGroupsContent>
          <TitleTwo>Meus Grupos</TitleTwo>
          <MyGroupsList />
        </MyGroupsContent>
      </MyGroupsContainer>

      <ContentTwo>
        <TitleTwo>Descobrir grupos</TitleTwo>
        <SearchGroup />
      </ContentTwo>

      <GroupsList />

      <ReturnTopIcon onClick={() => window.scrollTo(0, 0)}>
        <IoIosArrowDropupCircle />
      </ReturnTopIcon>

      <BubbleOne />
      <BubbleTwo />
      <BubbleThree />
    </Container>
  );
};

export default Groups;
