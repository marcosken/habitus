import styled from "styled-components";
import IconArrow from "./../../assets/img/arrow-down.svg";
import ImgBanner from "./../../assets/img/banner-groups-desktop.svg";

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const ContentOne = styled.div`
  display: flex;
  align-items: center;
  margin-left: 1rem;
`;

export const Title = styled.h1`
  font-family: var(--font1);
  font-weight: bold;
  color: var(--mono4);
  font-size: 40px;

  @media (min-width: 1024px) {
    font-size: 3rem;
`;

export const ContentTwo = styled.div`
  width: 90%;
  padding-bottom: 1rem;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-around;
  margin: 0 auto;

  @media (min-width: 1024px) {
    width: 70%;
  }
`;

export const TitleTwo = styled.h1`
  font-family: var(--font1);
  font-weight: bold;
  color: var(--mono4);
  font-size: 1.8rem;
  text-align: center;

  @media (min-width: 768px) {
    padding-top: 0;
  }
`;

export const ArrowIcon = styled.div`
  background: url(${IconArrow}) no-repeat center;
  background-size: contain;
  width: 36px;
  height: 32px;
  margin: 2rem 0 0 0.5rem;

  @media (min-width: 1024px) {
    width: 43px;
    height: 39px;
    margin: 3rem 0 0 0.5rem;
  }
`;

export const MyGroupsContainer = styled.div`
  display: flex;
  justify-content: space-evenly;
  padding: 2rem 0;
`;

export const MyGroupsContent = styled.div``;

export const BannerDesktop = styled.div`
  display: none;
  @media (min-width: 1024px) {
    display: flex;
    width: 330px;
    flex-direction: column;
    justify-content: center;
  }
`;

export const ImgBannerDesktop = styled.div`
  background: url(${ImgBanner}) no-repeat center;
  background-size: contain;
  width: 300px;
  height: 250px;
  display: flex;
  align-items: flex-end;
  width: 100%;
`;

export const MsgBanner = styled.h2`
  font-family: var(--font1);
  color: var(--mono4);
  font-size: 1.2rem;
  text-align: center;
`;

export const ReturnTopIcon = styled.div`
  color: var(--mono4);
  font-size: 3rem;
  margin: 1rem auto;
  cursor: pointer;

  :hover {
    opacity: 0.8;
  }
`;

export const BtnCloseModal = styled.button`
  border: none;
  background: transparent;
  color: var(--mono8);
  font-size: 2rem;
  font-family: var(--font3);
  cursor: pointer;
  :hover {
    font-weight: bold;
  }
`;

export const TitleNewGroup = styled.h2`
  font-family: var(--font3);
  color: var(--mono4);
  text-align: center;
  font-size: 2.5rem;
  margin: 3rem 0;
  @media (min-width: 1024px) {
    margin: 2rem 0;
    font-size: 4rem;
  }
`;

export const Section = styled.section`
  width: 100%;
  @media (min-width: 768px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
  }
  @media (min-width: 1024px) {
    flex-direction: row;
  }
`;

export const Figure = styled.figure`
  display: none;
  @media (min-width: 1024px) {
    display: block;
    width: 80%;
    margin: 0 auto;
  }
`;

export const Image = styled.img`
  width: 100%;
`;

export const BubbleOne = styled.div`
  width: 90px;
  height: 90px;
  border-radius: 50%;
  background-color: var(--mono2);
  position: fixed;
  left: calc(100% - 40px);
  top: 20%;
  z-index: -1;

  @media (min-width: 1024px) {
    width: 180px;
    height: 180px;
    left: calc(100% - 90px);
    top: 20%;
  }
`;

export const BubbleTwo = styled.div`
  width: 160px;
  height: 160px;
  border-radius: 50%;
  background-color: var(--mono2);
  position: fixed;
  left: calc(0% - 80px);
  top: calc(50% - 80px);
  z-index: -1;

  @media (min-width: 1024px) {
    width: 400px;
    height: 400px;
    left: calc(0% - 270px);
    top: calc(50% - 100px);
  }
`;

export const BubbleThree = styled.div`
  width: 148px;
  height: 148px;
  border-radius: 50%;
  background-color: var(--mono2);
  position: fixed;
  left: calc(100% - 74px);
  top: 80%;
  z-index: -1;

  @media (min-width: 1024px) {
    width: 320px;
    height: 320px;
    left: calc(100% - 200px);
    top: calc(100% - 200px);
  }
`;
