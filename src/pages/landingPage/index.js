import { Button } from "../../components/button";
import { useHistory } from "react-router";
import {
  Apresentation,
  CheckList,
  CheckListContainer,
  CheckListItem,
  Container,
  Figure1,
  Figure2,
  Figure3,
  Figure4,
  Figure5,
  Footer,
  Image1,
  Image2,
  Image3,
  Image4,
  Image5,
  MainContainer,
  Welcome,
} from "./styles";
import { GiCheckMark } from "react-icons/gi";
import { useUser } from "../../providers/user";
import { Redirect } from "react-router-dom";

import image1 from "../../assets/img/landing/landing1.svg";
import image2 from "../../assets/img/landing/landing2.svg";
import image3 from "../../assets/img/landing/landing3.svg";
import image4 from "../../assets/img/landing/landing4.svg";
import image5 from "../../assets/img/landing/landing5.svg";

const LandingPage = () => {
  const { isLogged } = useUser();

  const history = useHistory();
  const counterUsers = 1234;

  if (isLogged) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Button classNameProp="login" onClick={() => history.push("/login")}>
        LOGIN
      </Button>
      <MainContainer>
        <Welcome>
          Bem vindo ao
          <span> HABITUS!</span>
        </Welcome>
        <Figure1>
          <Image1 src={image1} alt="Homem sobre caminho galático" />
        </Figure1>
        <Apresentation>
          Transforme sua vida em apenas<span> 25 dias</span>.
          <br />
          Criando atividades e transformando-as em<span> novos hábitos</span>.
          <br />
          <br />
          Além de poder se conectar com os<span> {counterUsers}</span> usuários
          cadastrados.
          <br />
          <br />E encontrar grupos que compartilham do mesmo interesse.
        </Apresentation>
        <CheckListContainer>
          <Figure5>
            <Image5 src={image5} alt="Homem olhando lista" />
          </Figure5>
          <Figure4>
            <Image4 src={image4} alt="Mulher interagindo com lista" />
          </Figure4>
          <CheckList>
            <CheckListItem>
              <div>
                <GiCheckMark />
                <p>Passear com o cachorro</p>
              </div>
            </CheckListItem>
            <CheckListItem>
              <div>
                <GiCheckMark />
                <p>Meditar</p>
              </div>
            </CheckListItem>
            <CheckListItem>
              <div>
                <GiCheckMark size="22" />
                <p>Ler 2 capítulos de ”Como gerar novos hábitos”</p>
              </div>
            </CheckListItem>
            <CheckListItem>
              <div>
                <GiCheckMark />
                <p>Levar a vó no jiu-jitsu</p>
              </div>
            </CheckListItem>
          </CheckList>
        </CheckListContainer>
      </MainContainer>
      <Footer>
        <Button
          classNameProp="createAcc"
          onClick={() => history.push("/register")}
        >
          CRIE SUA CONTA!
        </Button>
        <Figure2>
          <Image2 src={image2} alt="Menina com fone de ouvido e celular" />
        </Figure2>
        <Figure3>
          <Image3 src={image3} alt="menina meditando" />
        </Figure3>
      </Footer>
    </Container>
  );
};

export default LandingPage;
