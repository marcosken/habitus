import styled from "styled-components";

export const Container = styled.main`
  font-family: var(--font1);
  color: var(--mono4);
  width: 100vw;
  height: calc(100vh - 64px);
  display: flex;
  align-content: space-between;
  flex-wrap: wrap;
  @media (min-width: 768px) {
    height: 85vh;
  }
`;

export const MainContainer = styled.section`
  padding: 0 1rem;
  display: flex;
  flex-wrap: wrap;
  align-items: flex-end;
  justify-content: center;
  width: 100%;
  height: 80%;
  align-content: space-evenly;

  @media (min-width: 768px) {
    margin: 2rem 0 0 100px;
    width: 50%;
    height: 70%;
    margin-left: 9rem;
    justify-content: start;
  }
`;

export const Welcome = styled.h1`
  width: 155px;
  font-size: 1.4rem;
  font-weight: bold;
  /* margin-bottom: 1rem; */
  margin-right: 2rem;
  position: relative;
  top: -15px;
  span {
    font-size: 1.8rem;
    font-weight: 800;
  }
  @media (min-width: 768px) {
    top: 0;
    font-size: 2rem;
    width: 340px;
    span {
      font-size: 3rem;
    }
  }
`;

export const Apresentation = styled.p`
  padding-top: 1rem;
  font-size: 1rem;
  span {
    font-size: 1.1rem;
    font-weight: bold;
  }
  @media (min-width: 768px) {
    font-size: 1.1rem;
    width: 80%;
    span {
      font-size: 1.2rem;
    }
  }
`;

export const CheckListContainer = styled.div`
  display: flex;
  align-items: center;
  margin-top: 1rem;
  justify-content: center;
  width: 100%;
  @media (min-width: 768px) {
    justify-content: start;
  }
`;

export const CheckList = styled.ul``;

export const CheckListItem = styled.li`
  width: 180px;
  font-size: 0.8rem;
  margin-top: 0.5rem;
  margin-bottom: 20px;
  div {
    display: flex;
    margin-left: 0.5rem;
  }
  p {
    margin-left: 0.5rem;
  }

  @media (min-width: 848px) {
    width: 300px;
    font-size: 1rem;
    margin-top: 1.5rem;
  }
`;

export const Footer = styled.div`
  height: 110px;
  width: 100%;
  background-color: var(--mono2);
  display: flex;
  align-items: center;
  justify-content: center;

  @media (min-width: 768px) {
    margin-top: 0;
    justify-content: start;
    position: absolute;
    bottom: 0;
  }
`;

export const Figure1 = styled.figure`
  position: relative;
  top: 10px;
  display: flex;
  justify-content: center;

  @media (min-width: 768px) {
    position: absolute;
    top: 0;
    right: 0;
  }
`;
export const Image1 = styled.img`
  width: 130px;
  @media (min-width: 768px) {
    width: 40vw;
    height: 32vw;
    top: 14vh;
    right: 7vw;
    position: absolute;
  }
`;

export const Figure2 = styled.figure`
  display: none;
  @media (min-width: 900px) {
    display: flex;
    position: absolute;
    right: 0;
    bottom: 0;
  }
`;
export const Image2 = styled.img`
  display: none;
  @media (min-width: 768px) {
    width: 10vw;
    max-width: 200px;
    max-height: 365px;
    position: absolute;
    right: 5vw;
    bottom: 0;
    display: flex;
  }
`;

export const Figure3 = styled.figure`
  display: none;
  @media (min-width: 900px) {
    bottom: 0;
    right: 0;
    position: absolute;
    display: flex;
  }
`;
export const Image3 = styled.img`
  display: none;
  @media (min-width: 768px) {
    width: 9vw;
    max-width: 180px;
    max-height: 240px;
    bottom: 0;
    right: 40vw;
    position: absolute;
    display: flex;
  }
`;

export const Figure4 = styled.figure`
  display: none;
  @media (min-width: 768px) {
    display: flex;
  }
`;
export const Image4 = styled.img`
  display: none;
  @media (min-width: 768px) {
    display: flex;
    width: 180px;
  }
`;

export const Figure5 = styled.figure`
  @media (min-width: 768px) {
    display: none;
  }
`;
export const Image5 = styled.img`
  @media (min-width: 768px) {
    display: none;
  }
`;
