import FormLogin from "../../components/formLogin";
import { Container, Background, Content } from "./styles";
import loginImage from "../../assets/img/login.svg";
import { useUser } from "../../providers/user";
import { Redirect } from "react-router-dom";

const Login = () => {
  const { isLogged } = useUser();

  if (isLogged) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Background>
        <figure>
          <img src={loginImage} alt="loginImage" />
        </figure>
      </Background>
      <Content>
        <h1>LOGIN</h1>
        <FormLogin />
      </Content>
    </Container>
  );
};

export default Login;
