import FormRegister from "../../components/FormRegister";
import { Background, Container, Content } from "./styles";
import registerImage from "../../assets/img/register.svg";
import { useUser } from "../../providers/user";
import { Redirect } from "react-router-dom";

const Register = () => {
  const { isLogged } = useUser();

  if (isLogged) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Content>
        <h1>CADASTRO</h1>
        <FormRegister />
      </Content>
      <Background>
        <figure>
          <img src={registerImage} alt="registerImage" />
        </figure>
      </Background>
    </Container>
  );
};

export default Register;
