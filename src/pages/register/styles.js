import styled from "styled-components";

export const Container = styled.div`
  height: 90vh;
  display: flex;
`;

export const Background = styled.div`
  width: 100vw;
  position: absolute;
  z-index: -1;
  opacity: 0.2;
  margin-top: 3rem;
  display: flex;
  justify-content: center;
  img {
    max-width: 800px;
  }
  figure {
    img {
      width: 100%;
    }
  }

  /* @media (min-width: 768px) {
    figure {
      width: 50vw;
    }
  } */

  /* @media (min-width: 1024px) {
    figure {
      width: 90%;
    } */
  }

  @media (min-width: 1024px) {
    position: relative;
    z-index: 0;
    opacity: 1;
  }
`;

export const Content = styled.div`
  width: 100vw;
  display: flex;
  flex-direction: column;
  align-items: center;

  h1 {
    color: var(--mono4);
    font-family: var(--font3);
    font-size: 3rem;
    margin-top: 3rem;
    margin-bottom: 1rem;
  }

  button:hover {
    filter: brightness(110%);
    cursor: pointer;
    background-color: var(--mono2);
    box-shadow: 10px 10px 28px 0px rgba(0, 0, 0, 0.75);
  }
`;
