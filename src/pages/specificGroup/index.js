import { useEffect, useState } from "react";

import { useUser } from "../../providers/user";
import { Redirect } from "react-router-dom";
import GroupDetails from "../../components/groupDetails";
import { useParams } from "react-router-dom";
import { api } from "../../services/api";
import { useMyGroups } from "../../providers/myGroups";
import loading from "../../assets/img/loading.svg";
import { Container } from './styles';

const SpecificGroup = () => {
  const { isLogged } = useUser();
  const { myGroups } = useMyGroups();
  const [userIsInGroup, setUserIsInGroup] = useState(true);
  const params = useParams();
  const [groupSpecific, setGroupSpecific] = useState({});
  const handleIsInGroup = () => {
    for (let i = 0; i < myGroups.length; i++) {
      if (groupSpecific.id === myGroups[i].id) {
        return setUserIsInGroup(true);
      }
    }
    return setUserIsInGroup(false);
  };

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    handleIsInGroup();
  }, [myGroups]);

  useEffect(() => {
    api
      .get(`/groups/${params.idGroup}/`)
      .then((res) => {
        setIsLoading(false);
        setGroupSpecific(res.data)
      });
  }, []);

  if (!isLogged) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>
      {isLoading?
      <img src={loading} alt=""/>
      :
      <GroupDetails userIsInGroup={userIsInGroup} actual={groupSpecific} />
      }
    </Container>
  );
};

export default SpecificGroup;
