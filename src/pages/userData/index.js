import FormEditUser from "../../components/formEditUser";
import { useUser } from "../../providers/user";
import { Redirect } from "react-router-dom";

import { Container, FormContainer, Background } from "./styles";
import userImage from "../../assets/img/user.png";

const UserData = () => {
  const { isLogged } = useUser();

  if (!isLogged) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>
      <Background>
        <figure>
          <img src={userImage} alt="userImage" />
        </figure>
      </Background>
      <FormContainer>
        <h1>Dados do Usuário</h1>
        <FormEditUser />
      </FormContainer>
    </Container>
  );
};

export default UserData;
