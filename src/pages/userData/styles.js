import styled from "styled-components";

export const Container = styled.div`
  height: calc(100vh - var(--heightHeaderMobile));
  position: relative;
`;

export const FormContainer = styled.div`
  position: absolute;
  width: 80%;
  padding: 2rem 1.5rem;
  right: 10vw;
  top: 10%;
  box-shadow: 0px 0px 11px 5px rgba(0, 0, 0, 0.41);
  background-color: white;
  border-radius: 1rem;

  h1 {
    color: var(--mono4);
    font-size: 2rem;
    text-align: center;
    font-family: var(--font3);
    margin-bottom: 3rem;
  }

  form {
    text-align: center;
    margin: 0 auto;
    p {
      font-family: var(--font3);
    }
    div {
      margin-bottom: 1rem;
    }
  }

  button {
    background-color: var(--mono4);
    color: var(--mono8);
    width: 100%;
    height: 50px;
    font-size: 1rem;
    font-family: var(--font3);
    border: 2px solid var(--mono4);
    border-radius: 0.2rem;
    margin-top: 1rem;
    :hover {
      cursor: pointer;
      background-color: transparent;
      color: var(--mono4);
      box-shadow: 0px 0px 20px 0px var(--mono1);
    }
  }

  @media (min-width: 768px) {
    width: 50%;
    top: 10%;
    left: 50%;
    transform: translateX(-50%);
  }
  @media (min-width: 1024px) {
    width: 35%;
    top: 50%;
    transform: translateY(-50%);
  }
`;

export const Background = styled.div`
  display: none;

  @media (min-width: 1024px) {
    display: flex;
    width: 50%;
    height: 100%;
    justify-content: center;
    align-items: center;
    figure {
      width: 70%;
      img {
        width: 100%;
      }
    }
  }
`;
