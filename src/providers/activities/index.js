import { useContext, useState, createContext, useEffect} from 'react';
import { api, localToken } from '../../services/api';
import { toast } from 'react-toastify';
import { format, addDays } from 'date-fns';


const ActivitiesContext = createContext();

export const ActivitiesProvider = ({children}) => {

    const [currDate, setCurrDate] = useState((new Date()));
    
    const [nextDate, setNextDate] = useState(addDays(currDate, 1));

    const [formattedNextDate, setFormattedNextDate] = useState(format(nextDate,"yyyy-MM-dd") + "T00:00:00Z");


    
   
    const createActivity = (payload, groupID) => {
        //TODO: ADD logic to realization_time
        api
        .post(`/activities/`, {...payload, group: groupID})
        .then((res) => {
            toast.success("Você criou uma atividade!");
            api
            .get(`/groups/${groupID}/`)
        })
        .catch(err => console.log(err));

    }

    const deleteActivity = (actvID) => {
        api
        .delete(`/activities/${actvID}/`)
        .then(_ => toast.warn("Você deletou uma atividade!"));
    }

    
  

    const editActivity = (payload, activityID) => {
        console.log(payload)
        api
        .patch(`/activities/${activityID}/`, payload)
        .catch(err => console.log(err));
    }

    const checkActivity = (activity) => {
        console.log(formattedNextDate);
        const { title, group} = activity;
        api
        .delete(`/activities/${activity.id}/`)
        .then(() => {
            toast.success("Atividade completada!")
            api
            .post(`/activities/`, {title: title, group: group, realization_time: formattedNextDate})
            .then(() => {
                toast.success("Nova atividade!");
            })


        })


    }





    return(
        <ActivitiesContext.Provider 
        value={{ 
        createActivity, 
        deleteActivity,
        editActivity,
        checkActivity}}>
            {/* TODO: ADD VALUES */}
            {children}
        </ActivitiesContext.Provider>
    )


}

export const useActivities = () => useContext(ActivitiesContext);
