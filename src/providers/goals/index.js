import { useContext, useState, createContext } from "react";
import { api, localToken, userID } from "../../services/api";
import { useEffect } from "react";
import { toast } from "react-toastify";

const GoalsContext = createContext();

export const GoalsProvider = ({ children }) => {

  const createGoal = (payload, groupID) => {
    api
      .post("/goals/", { ...payload, how_much_achieved: 0, group: groupID })
      .then((res) => {
        toast.success("Você criou uma meta!");
      })
      .catch((err) => console.log(err));
  };

  const progressInGoal = (currProgress, goalID) => {
    if (!currProgress >= 100) {
      api
        .patch(`/goals/${goalID}/`, { how_much_achieved: currProgress + 4 })
        .then((res) => {
          if (res.data.how_much_achieved >= 100) {
            api
              .patch(`/goals/${goalID}`, { achieved: true })
              .catch((err) => console.log(err));
          }
        })
        .catch((err) => console.log(err));
    }
  };


  const deleteGoal = (goalID) => {
    api
      .delete(`/goals/${goalID}/`)
      .then((_) => toast.warn("Você deletou uma meta!"));
  };

  const editGoal = (payload, goalID) => {
    api.patch(`/goals/${goalID}/`, payload).catch((err) => console.log(err));
  };

  return (
    <GoalsContext.Provider
      value={{
        createGoal,
        progressInGoal,
        deleteGoal,
        editGoal,
      }}
    >
      {children}
    </GoalsContext.Provider>
  );
};

export const useGoals = () => useContext(GoalsContext);
