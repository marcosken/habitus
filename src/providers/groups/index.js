import { useContext, useState, createContext, useEffect } from "react";
import { api, userID } from "../../services/api";

const GroupsContext = createContext();

export const GroupsProvider = ({ children }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [groups, setGroups] = useState(
    JSON.parse(localStorage.getItem("groups")) || []
  );
  const [page, setPage] = useState(1);
  const [countGroups, setCountGroups] = useState(0);

  const [searchResults, setSearchResults] = useState([]);
  const [repeatedElements, setRepeatedElements] = useState([]);

  const searchGroups = (input) => {
    setRepeatedElements(
      groups.filter(
        (el) =>
          el.name.toUpperCase().includes(input.toUpperCase()) ||
          el.category.toUpperCase().includes(input.toUpperCase())
      )
    );
    setSearchResults(
      repeatedElements.filter(
        (ele, ind) =>
          ind ===
          repeatedElements.findIndex(
            (elem) => elem.id === ele.id && elem.name === ele.name
          )
      )
    );
  };

  useEffect(() => {
    api
      .get(`/groups/?page=${page}`)
      .then((res) => {
        setGroups(res.data.results);
        setCountGroups(res.data.count);
      })
      .catch((err) => console.log("oi"));
  }, [page]);

  const isGroupCreator = (group) => {
    return group.creator?.id === userID ? true : false;
  };

  return (
    <GroupsContext.Provider
      value={{
        groups,
        setGroups,
        searchGroups,
        searchResults,
        isLoading,
        setIsLoading,
        setSearchResults,
        isGroupCreator,
        page,
        setPage,
        countGroups,
      }}
    >
      {children}
    </GroupsContext.Provider>
  );
};

export const useGroups = () => useContext(GroupsContext);
