import { useContext, useState, createContext, useCallback } from "react";
import { api, userID } from "../../services/api";
import { toast } from "react-toastify";

const HabitsContext = createContext();

export const HabitsProvider = ({ children }) => {
  const [habits, setHabits] = useState(
    JSON.parse(localStorage.getItem("habits")) || []
  );
  const [isLoading, setIsLoading] = useState(true);

  const loadHabits = useCallback(async (token) => {
    api
      .get("/habits/personal/", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        setHabits(res.data);
        localStorage.setItem("habits", JSON.stringify([...res.data]));
        setIsLoading(false);
      })
      .catch((err) => console.log("erro"));
  }, []);

  const createHabit = (payload, token, id) => {
    setIsLoading(true);
    api
      .post(
        "/habits/",
        {
          ...payload,
          frequency: "Diária",
          how_much_achieved: 0,
          user: id,
        },
        { headers: { Authorization: `Bearer ${token}` } }
      )
      .then((res) => {
        toast.success("Hábito adicionado!");
        localStorage.setItem("habits", JSON.stringify([...habits, res.data]));
        setHabits([...habits, res.data]);
        setIsLoading(false);
      })
      .catch((err) => toast.error("Algo deu errado!"));
  };

  const deleteHabit = (habitID, token) => {
    let currHabitIndex = habits.findIndex((el) => el.id === habitID);
    habits.splice(currHabitIndex, 1);
    setHabits(habits.filter((el) => el.id !== habitID));

    setIsLoading(true);

    api
      .delete(`/habits/${habitID}/`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(() => {
        toast.warn("Hábito excluído!");
        localStorage.setItem("habits", JSON.stringify(habits));
        setIsLoading(false);
      })

      .catch((err) => console.log(err));
  };

  const progressInHabit = (habitID, currProgress, token) => {
    let selectedHabit = habits.find((el) => el.id === habitID);
    const selectedHabitIndex = habits.findIndex(
      (el) => el.id === selectedHabit.id
    );
    if (selectedHabit.how_much_achieved >= 96) {
      selectedHabit.how_much_achieved = 100;
      selectedHabit.achieved = true;
      setHabits(habits.splice(selectedHabitIndex, 1, selectedHabit));
      localStorage.setItem("habits", JSON.stringify([...habits]));
      setHabits([...habits]);
      api
        .patch(
          `/habits/${habitID}/`,
          {
            how_much_achieved: 100,
            achieved: true,
          },
          { headers: { Authorization: `Bearer ${token}` } }
        )
        .then(() => {
          toast.success("Parabéns! Você desenvolveu um novo hábito!");
          api
            .get(`/habits/personal/`, {
              headers: { Authorization: `Bearer ${token}` },
            })
            .then((res) => {
              setHabits([...habits]);
              localStorage.setItem("habits", JSON.stringify([...habits]));
            });
        })
        .catch((err) => console.log(err));
    } else {
      selectedHabit.how_much_achieved = currProgress + 4;

      setHabits(habits.splice(selectedHabitIndex, 1, selectedHabit));
      localStorage.setItem("habits", JSON.stringify([...habits]));
      setHabits([...habits]);
      api
        .patch(
          `/habits/${habitID}/`,
          { how_much_achieved: currProgress + 4 },
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        )
        .catch((err) => console.log(err));
    }
  };

  const editHabit = (payload, habitID) => {
    api.patch(`/habits/${habitID}/`, payload).catch((err) => console.log(err));
  };

  return (
    <HabitsContext.Provider
      value={{
        habits,
        setHabits,
        createHabit,
        deleteHabit,
        progressInHabit,
        editHabit,
        isLoading,
        setIsLoading,
        loadHabits,
      }}
    >
      {children}
    </HabitsContext.Provider>
  );
};

export const useHabits = () => useContext(HabitsContext);
