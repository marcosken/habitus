import { ActivitiesProvider } from "./activities";
import { GoalsProvider } from "./goals";
import { GroupsProvider } from "./groups";
import { HabitsProvider } from "./habits";
import { MyGroupsProvider } from "./myGroups";
import { UserProvider } from "./user";

const Providers = ({ children }) => {
  return (
    <GoalsProvider>
      <ActivitiesProvider>
        <GroupsProvider>
          <HabitsProvider>
            <UserProvider>
              <MyGroupsProvider>{children}</MyGroupsProvider>
            </UserProvider>
          </HabitsProvider>
        </GroupsProvider>
      </ActivitiesProvider>
    </GoalsProvider>
  );
};

export default Providers;
