import { createContext, useState, useContext, useCallback } from "react";
import { api, userID } from "../../services/api";
import { toast } from "react-toastify";

const MyGroupsContext = createContext();

export const MyGroupsProvider = ({ children }) => {
  const [myGroups, setMyGroups] = useState(
    JSON.parse(localStorage.getItem("myGroups")) || []
  );
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const MAX_NUMBER_OF_GROUPS = 4;

  const loadMyGroups = useCallback((token) => {
    api
      .get("/groups/subscriptions/", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        setMyGroups(res.data);
        localStorage.setItem("myGroups", JSON.stringify([...res.data]));
        setIsLoading(false);
      })
      .catch((err) => console.log(err));
  }, []);

  const addToMyGroups = (data, token) => {
    if (myGroups.length < MAX_NUMBER_OF_GROUPS) {
      api
        .post("/groups/", data, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((res) => {
          toast.success("Grupo criado com sucesso!");
          setMyGroups([...myGroups, res.data]);
        })
        .catch((err) => console.log(err));
    } else {
      setError(true);
    }
  };

  const subscribeToGroup = (group, token) => {
    console.log(token);
    if (myGroups.length < MAX_NUMBER_OF_GROUPS) {
      api
        .post(`groups/${group.id}/subscribe/`, null, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then((res) => {
          toast.success("Inscrição realizada com sucesso!");
          setMyGroups([...myGroups, res.data]);
          localStorage.setItem("myGroups", JSON.parse([...myGroups]));
        })
        .catch((err) => toast.error("Você já é inscrito nesse grupo!"));
    } else {
      toast.warn(
        `O limite de participação em ${MAX_NUMBER_OF_GROUPS} grupos já foi atingido.`
      );
      setError(true);
    }
  };

  const editGroup = (payload, groupID) => {
    let currGroup = myGroups.find((el) => el.id === groupID);

    const currGroupIndex = myGroups.findIndex((el) => el.id === groupID);

    const { name, description, category } = payload;

    currGroup = {
      ...currGroup,
      name: name,
      description: description,
      category: category,
    };

    myGroups.splice(currGroupIndex, 1, currGroup);

    localStorage.setItem("myGroups", JSON.stringify([...myGroups]));

    api
      .patch(`groups/${groupID}/`, payload)
      .then(() => {
        api.get(`/groups/subscriptions/`).then((res) => {
          setMyGroups([res.data]);
        });
      })
      .catch((err) => console.log(err));
  };

  // const isGroupCreator = (group) => {
  //   return group.creator.id === userID ? true : false;
  // };

  return (
    <MyGroupsContext.Provider
      value={{
        myGroups,
        addToMyGroups,
        subscribeToGroup,
        error,
        setError,
        editGroup,
        // isGroupCreator,
        isLoading,
        loadMyGroups,
        MAX_NUMBER_OF_GROUPS,
      }}
    >
      {children}
    </MyGroupsContext.Provider>
  );
};

export const useMyGroups = () => useContext(MyGroupsContext);
