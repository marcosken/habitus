import { createContext, useContext, useEffect, useState } from "react";
import { api } from "../../services/api";
import { toast } from "react-toastify";
// import { userID } from "../../services/api";
import jwt_decode from "jwt-decode";

const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [isLogged, setIsLogged] = useState(
    localStorage.getItem("authToken") ? true : false
  );
  const [accessToken, setAccessToken] = useState(
    localStorage.getItem("authToken") || ""
  );
  const [user, setUser] = useState(
    JSON.parse(localStorage.getItem("user")) || ""
  );
  const [userID, setUserID] = useState(
    JSON.parse(localStorage.getItem("userID")) || ""
  );

  const loadUser = (userId) => {
    api
      .get(`/users/${userId}/`)
      .then((res) => {
        setUser(res.data);
        localStorage.setItem("user", JSON.stringify(res.data));
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    const token = localStorage.getItem("authToken") || "";
    if (userID) {
      loadUser(userID);
    }

    if (!!token) {
      return setIsLogged(true);
    }
  }, [isLogged, userID]);

  const login = (payload, history) => {
    api
      .post(`/sessions/`, { ...payload })
      .then((res) => {
        setIsLogged(true);
        const token = res.data.access;
        setAccessToken(token);
        localStorage.setItem("authToken", token);

        const decoded = jwt_decode(token);
        setUserID(decoded.user_id);
        localStorage.setItem("userID", JSON.stringify(decoded.user_id));

        history.push("/dashboard");
        toast.success("Bem-vindo!!! Login feito com sucesso!");
      })
      .catch((err) => {
        toast.error("Usuário ou senha inválidos!");
      });
  };

  const registerNewUser = (payload, history) => {
    api
      .post(`/users/`, { ...payload })
      .then((res) => {
        toast.success("Cadastro feito com sucesso!");
        history.push("/login");
      })
      .catch((err) => toast.error("Esse nome de usuário já existe!"));
  };

  return (
    <UserContext.Provider
      value={{
        login,
        registerNewUser,
        isLogged,
        loadUser,
        user,
        userID,
        accessToken,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => useContext(UserContext);
