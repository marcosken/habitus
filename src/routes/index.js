import LandingPage from "../pages/landingPage";
import Login from "../pages/login";
import Register from "../pages/register";
import Dashboard from "../pages/dashboard";
import Groups from "../pages/groups";
import UserData from "../pages/userData";
import { Switch, Route } from "react-router-dom";
import Error from "../pages/error";
import SpecificGroup from "../pages/specificGroup";

const Routes = () => (
  <Switch>
    <Route exact path="/" component={LandingPage} />
    <Route path="/login" component={Login} />
    <Route path="/register" component={Register} />
    <Route path="/dashboard" component={Dashboard} />
    <Route path="/groups" component={Groups} />
    <Route path="/userData" component={UserData} />
    <Route path="/specificGroup/:idGroup" component={SpecificGroup} />
    <Route component={Error} />
  </Switch>
);

export default Routes;
