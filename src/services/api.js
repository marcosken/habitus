import axios from "axios";
import jwtDecode from "jwt-decode";

export const api = axios.create({
  baseURL: "https://kenzie-habits.herokuapp.com",
});

export const localToken = localStorage.getItem("authToken") || "";

const decodedToken = localToken === "" ? "" : jwtDecode(localToken);

export const userID = decodedToken.user_id;

// api.defaults.headers.authorization =
//   localToken === "" ? "" : `Bearer ${localToken}`;
